package com.palidinodh.cache.store.fs.jagex;

import com.palidinodh.cache.store.fs.Archive;
import com.palidinodh.cache.store.fs.Container;
import com.palidinodh.cache.store.fs.Index;
import com.palidinodh.cache.store.fs.Storage;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.index.IndexData;
import com.palidinodh.cache.store.util.CachedArchive;
import com.palidinodh.cache.store.util.Crc32;
import com.palidinodh.cache.store.util.Ints;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DiskStorage implements Storage {

  private static final String MAIN_FILE_CACHE_DAT = "main_file_cache.dat2";
  private static final String MAIN_FILE_CACHE_IDX = "main_file_cache.idx";

  private final File folder;
  private final DataFile data;
  private final IndexFile index255;
  private final List<IndexFile> indexFiles = new ArrayList<>();
  private CachedArchive cachedArchive = new CachedArchive();

  public DiskStorage(File folder) throws IOException {
    this.folder = folder;
    this.data = new DataFile(new File(folder, MAIN_FILE_CACHE_DAT));
    this.index255 = new IndexFile(255, new File(folder, MAIN_FILE_CACHE_IDX + "255"));
  }

  @Override
  public void init(Store store) throws IOException {
    for (var i = 0; i < index255.getIndexCount(); ++i) {
      store.addIndex(i);
      getIndex(i);
    }
    if (store.getIndexes().size() != indexFiles.size()) {
      throw new RuntimeException(
          "Index counts don't match: " + store.getIndexes().size() + "/" + indexFiles.size());
    }
  }

  @Override
  public void close() throws IOException {
    data.close();
    index255.close();
    for (var indexFile : indexFiles) {
      indexFile.close();
    }
  }

  @Override
  public void load(Store store) throws IOException {
    for (var index : store.getIndexes()) {
      loadIndex(index);
    }
  }

  @Override
  public byte[] loadArchive(Archive archive) throws IOException {
    var index = archive.getIndex();
    var indexFile = getIndex(index.getId());
    if (indexFile.getIndexFileId() != index.getId()) {
      throw new RuntimeException(
          "Indexes don't match: " + indexFile.getIndexFileId() + "/" + index.getId());
    }
    var entry = indexFile.read(archive.getArchiveId());
    if (entry == null) {
      throw new IOException(
          "Can't read archive from " + index.getId() + "/" + archive.getArchiveId());
    }
    if (entry.getId() != archive.getArchiveId()) {
      throw new RuntimeException(
          "Archives don't match: " + entry.getId() + "/" + archive.getArchiveId());
    }
    var archiveData = data.read(index.getId(), entry.getId(), entry.getSector(), entry.getLength());
    return archiveData;
  }

  @Override
  public void save(Store store) throws IOException {
    for (var i : store.getIndexes()) {
      saveIndex(i);
    }
  }

  @Override
  public void saveArchive(Archive a, byte[] archiveData) throws IOException {
    var index = a.getIndex();
    var indexFile = getIndex(index.getId());
    if (indexFile.getIndexFileId() != index.getId()) {
      throw new RuntimeException(
          "Indexes don't match: " + indexFile.getIndexFileId() + "/" + index.getId());
    }
    var res = data.write(index.getId(), a.getArchiveId(), archiveData);
    indexFile.write(new IndexEntry(indexFile, a.getArchiveId(), res.sector, res.compressedLength));
    var compression = archiveData[0];
    var compressedSize =
        Ints.fromBytes(archiveData[1], archiveData[2], archiveData[3], archiveData[4]);
    var length = 1 + 4 + compressedSize + (compression != CompressionType.NONE ? 4 : 0);
    a.setCrc(Crc32.getHash(archiveData, 0, length));
  }

  @Override
  public byte[] readIndex(int indexId) throws IOException {
    var entry = index255.read(indexId);
    var indexData =
        data.read(index255.getIndexFileId(), entry.getId(), entry.getSector(), entry.getLength());
    return indexData;
  }

  @Override
  public CachedArchive getCachedArchive() {
    return cachedArchive;
  }

  private IndexFile getIndex(int i) throws FileNotFoundException {
    for (var indexFile : indexFiles) {
      if (indexFile.getIndexFileId() != i) {
        continue;
      }
      return indexFile;
    }
    var indexFile = new IndexFile(i, new File(folder, MAIN_FILE_CACHE_IDX + i));
    indexFiles.add(indexFile);
    return indexFile;
  }

  private void loadIndex(Index index) throws IOException {
    var indexData = readIndex(index.getId());
    var res = Container.decompress(indexData, null);
    var data = res.data;
    var id = new IndexData();
    id.load(data);
    index.setProtocol(id.getProtocol());
    index.setRevision(id.getRevision());
    index.setNamed(id.isNamed());
    for (var ad : id.getArchives()) {
      var archive = index.addArchive(ad.getId());
      archive.setNameHash(ad.getNameHash());
      archive.setCrc(ad.getCrc());
      archive.setRevision(ad.getRevision());
      archive.setFiles(ad.getFiles());
    }
    index.setCrc(res.crc);
    index.setCompression(res.compression);
  }

  private void saveIndex(Index index) throws IOException {
    var indexData = index.toIndexData();
    var data = indexData.writeIndexData();
    var container = new Container(index.getCompression(), -1);
    container.compress(data, null);
    var compressedData = container.data;
    var res = this.data.write(index255.getIndexFileId(), index.getId(), compressedData);
    index255.write(new IndexEntry(index255, index.getId(), res.sector, res.compressedLength));
    index.setCrc(Crc32.getHash(compressedData));
  }
}
