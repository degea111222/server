package com.palidinodh.cache.store.index;

import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ArchiveData {

  private int id;
  private int nameHash;
  private int compression;
  private int crc;
  private int revision;
  private List<FileData> files;

  public ArchiveData(int id, int nameHash, int compression) {
    this.id = id;
    this.nameHash = nameHash;
    this.compression = compression;
  }

  public void addFile(FileData file) {
    if (files == null) {
      files = new LinkedList<>();
    }
    files.add(file);
  }
}
