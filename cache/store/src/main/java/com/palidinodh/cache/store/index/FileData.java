package com.palidinodh.cache.store.index;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FileData {

  private int id;
  private int nameHash;
  private byte[] contents;
}
