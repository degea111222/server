package com.palidinodh.cache.store.util;

public final class Djb2 {

  private Djb2() {}

  public static int hash(String str) {
    var hash = 0;
    for (var i = 0; i < str.length(); i++) {
      hash = str.charAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  public static long fullHash(String str) {
    var hash = 5381L;
    for (var i = 0; i < str.length(); i++) {
      hash = str.charAt(i) + ((hash << 5) + hash);
    }
    return hash;
  }
}
