package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.fs.jagex.DiskStorage;
import com.palidinodh.cache.store.index.ArchiveData;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.Getter;

public class Store implements Closeable {

  @Getter private final Storage storage;
  private final List<Index> indexes = new ArrayList<>();

  public Store(String folderName) throws IOException {
    storage = new DiskStorage(new File(folderName));
    storage.init(this);
    load();
  }

  public Store(File folder) throws IOException {
    storage = new DiskStorage(folder);
    storage.init(this);
    load();
  }

  public Store(Storage storage) throws IOException {
    this.storage = storage;
    storage.init(this);
    load();
  }

  @Override
  public void close() throws IOException {
    storage.close();
  }

  @Override
  public int hashCode() {
    var hash = 5;
    hash = 79 * hash + Objects.hashCode(indexes);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (Store) obj;
    if (!Objects.equals(indexes, other.indexes)) {
      return false;
    }
    return true;
  }

  public Index addIndex(int id) throws FileNotFoundException {
    for (var i : indexes) {
      if (i.getId() == id) {
        throw new IllegalArgumentException("index " + id + " already exists");
      }
    }
    var index = new Index(id, storage);
    indexes.add(index);
    return index;
  }

  public void removeIndex(Index index) {
    if (!indexes.contains(index)) {
      throw new IllegalArgumentException("Index " + index.getId() + " doesn't exist");
    }
    indexes.remove(index);
  }

  public void load() throws IOException {
    storage.load(this);
  }

  public void save() throws IOException {
    storage.save(this);
  }

  public List<Index> getIndexes() {
    return indexes;
  }

  public Index getIndex(IndexType type) {
    return getIndex(type.getId());
  }

  public Index getIndex(int id) {
    for (var i : indexes) {
      if (i.getId() != id) {
        continue;
      }
      return i;
    }
    return null;
  }

  public byte[] getIndexData(int indexId) throws IOException {
    return storage.readIndex(indexId);
  }

  public int getHighestArchiveId(IndexType indexType) {
    return getHighestArchiveId(indexType.getId());
  }

  public int getHighestArchiveId(int indexId) {
    return getIndex(indexId).getHighestArchiveId();
  }

  public int getArchiveId(IndexType indexType, String archiveName) {
    return getArchiveId(indexType.getId(), archiveName);
  }

  public int getArchiveId(int indexId, String archiveName) {
    var archive = getIndex(indexId).findArchiveByName(archiveName);
    return archive != null ? archive.getArchiveId() : -1;
  }

  public Archive getArchive(IndexType indexType, ConfigType configType) {
    return getArchive(indexType.getId(), configType.getId());
  }

  public Archive getArchive(IndexType indexType, int archiveId) {
    return getArchive(indexType.getId(), archiveId);
  }

  public Archive getArchive(int index, int archiveId) {
    return getIndex(index).getArchive(archiveId);
  }

  public byte[] getArchiveData(IndexType indexType, ConfigType configType) throws IOException {
    return getArchiveData(indexType.getId(), configType.getId());
  }

  public byte[] getArchiveData(IndexType indexType, int archiveId) throws IOException {
    return getArchiveData(indexType.getId(), archiveId);
  }

  public byte[] getArchiveData(int index, int archiveId) throws IOException {
    return storage.loadArchive(getArchive(index, archiveId));
  }

  public int getHighestFileId(IndexType indexType, ConfigType configType) {
    return getHighestFileId(indexType.getId(), configType.getId());
  }

  public int getHighestFileId(IndexType indexType, int archiveId) {
    return getHighestFileId(indexType.getId(), archiveId);
  }

  public int getHighestFileId(int indexId, int archiveId) {
    return getIndex(indexId).getArchive(archiveId).getHighestFileId();
  }

  public FsFile getFile(IndexType indexType, ConfigType configType, int fileId) throws IOException {
    return getFile(indexType.getId(), configType.getId(), fileId);
  }

  public FsFile getFile(IndexType indexType, int archiveId, int fileId) throws IOException {
    return getFile(indexType.getId(), archiveId, fileId);
  }

  public FsFile getFile(int indexId, int archiveId, int fileId) throws IOException {
    return getFile(indexId, archiveId, fileId, null);
  }

  public FsFile getFile(int indexId, int archiveId, int fileId, int[] keys) throws IOException {
    return getIndex(indexId).getArchive(archiveId).getFsFile(fileId, keys);
  }

  public byte[] getFileData(IndexType indexType, ConfigType configType, int fileId)
      throws IOException {
    return getFileData(indexType.getId(), configType.getId(), fileId);
  }

  public byte[] getFileData(IndexType indexType, ConfigType configType) throws IOException {
    return getFileData(indexType.getId(), configType.getId(), 0);
  }

  public byte[] getFileData(IndexType indexType, int archiveId, int fileId) throws IOException {
    return getFileData(indexType.getId(), archiveId, fileId);
  }

  public byte[] getFileData(IndexType indexType, int archiveId) throws IOException {
    return getFileData(indexType.getId(), archiveId, 0);
  }

  public byte[] getFileData(int indexId, int archiveId, int fileId) throws IOException {
    return getFileData(indexId, archiveId, fileId, null);
  }

  public byte[] getFileData(int indexId, int archiveId) throws IOException {
    return getFileData(indexId, archiveId, 0, null);
  }

  public byte[] getFileData(int indexId, int archiveId, int fileId, int[] keys) throws IOException {
    var file = getIndex(indexId).getArchive(archiveId).getFsFile(fileId, keys);
    return file != null ? file.getContents() : null;
  }

  public FsFile getFileByArchiveName(IndexType indexType, String archiveName, int fileId)
      throws IOException {
    return getFileByArchiveName(indexType.getId(), archiveName, fileId);
  }

  public FsFile getFileByArchiveName(int indexId, String archiveName, int fileId)
      throws IOException {
    return getFileByArchiveName(indexId, archiveName, fileId, null);
  }

  public FsFile getFileByArchiveName(int indexId, String archiveName, int fileId, int[] keys)
      throws IOException {
    return getIndex(indexId).findArchiveByName(archiveName).getFsFile(fileId, keys);
  }

  public byte[] getFileDataByArchiveName(IndexType indexType, String archiveName, int fileId)
      throws IOException {
    return getFileDataByArchiveName(indexType.getId(), archiveName, fileId);
  }

  public byte[] getFileDataByArchiveName(int indexId, String archiveName, int fileId)
      throws IOException {
    return getFileDataByArchiveName(indexId, archiveName, fileId, null);
  }

  public byte[] getFileDataByArchiveName(int indexId, String archiveName, int fileId, int[] keys)
      throws IOException {
    var file = getIndex(indexId).findArchiveByName(archiveName).getFsFile(fileId, keys);
    return file != null ? file.getContents() : null;
  }

  public List<FsFile> getFiles(IndexType indexType, ConfigType configType) throws IOException {
    return getFiles(indexType.getId(), configType.getId());
  }

  public List<FsFile> getFiles(IndexType indexType, int archiveId) throws IOException {
    return getFiles(indexType.getId(), archiveId);
  }

  public List<FsFile> getFiles(int indexId, int archiveId) throws IOException {
    return getFiles(indexId, archiveId, null);
  }

  public List<FsFile> getFiles(int indexId, int archiveId, int[] keys) throws IOException {
    return getIndex(indexId).getArchive(archiveId).getFsFiles(keys);
  }

  public void updateArchive(IndexType indexType, ArchiveData ad) throws IOException {
    updateArchive(indexType.getId(), ad);
  }

  public boolean updateArchive(int indexId, ArchiveData ad) throws IOException {
    return getIndex(indexId).updateArchive(ad);
  }

  public void updateDirectArchive(Archive ad, byte[] contents) throws IOException {
    if (ad == null) {
      return;
    }
    getIndex(ad.getIndex().getId()).updateDirectArchive(ad, contents);
  }

  public void copyArchive(Archive ad, boolean skipExistingFiles) throws IOException {
    if (ad == null) {
      return;
    }
    getIndex(ad.getIndex().getId()).copyArchive(ad, skipExistingFiles);
  }

  public void copyDirectArchive(Archive ad, boolean skipIfExists) throws IOException {
    if (ad == null) {
      return;
    }
    getIndex(ad.getIndex().getId()).copyDirectArchive(ad, skipIfExists);
  }

  public void copyArchiveFiles(Archive ad, List<Integer> fileIds) throws IOException {
    if (ad == null) {
      return;
    }
    getIndex(ad.getIndex().getId()).copyArchiveFiles(ad, fileIds);
  }
}
