package com.palidinodh.cache.store.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZip {

  private GZip() {}

  public static byte[] compress(byte[] bytes) throws IOException {
    var is = new ByteArrayInputStream(bytes);
    var bout = new ByteArrayOutputStream();
    try (var os = new GZIPOutputStream(bout)) {
      IOUtils.copy(is, os);
    }
    return bout.toByteArray();
  }

  public static byte[] decompress(byte[] bytes, int len) throws IOException {
    var os = new ByteArrayOutputStream();
    try (var is = new GZIPInputStream(new ByteArrayInputStream(bytes, 0, len))) {
      IOUtils.copy(is, os);
    }
    return os.toByteArray();
  }
}
