package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.index.FileData;
import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Archive {

  private final Index index;
  private final int archiveId;
  private int nameHash;
  private int crc;
  private int revision;
  private int compression;
  private List<FileData> files;

  public Archive(Index index, int archiveId) {
    this.index = index;
    this.archiveId = archiveId;
  }

  public Archive(Index index, int archiveId, int compression) {
    this.index = index;
    this.archiveId = archiveId;
    this.compression = compression;
  }

  @Override
  public int hashCode() {
    var hash = 7;
    hash = 47 * hash + archiveId;
    hash = 47 * hash + nameHash;
    hash = 47 * hash + revision;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (Archive) obj;
    if (archiveId != other.archiveId) {
      return false;
    }
    if (nameHash != other.nameHash) {
      return false;
    }
    return revision == other.revision;
  }

  public int getHighestFileId() {
    files.sort(Comparator.comparingInt(FileData::getId));
    return files.isEmpty() ? 0 : files.get(files.size() - 1).getId();
  }

  public FsFile getFsFile(int fileId) throws IOException {
    return getFsFile(fileId, null);
  }

  public FsFile getFsFile(int fileId, int[] keys) throws IOException {
    var list = getFsFiles(keys);
    for (var file : list) {
      if (file.getFileId() != fileId) {
        continue;
      }
      return file;
    }
    return null;
  }

  public List<FsFile> getFsFiles() throws IOException {
    return getFsFiles(null);
  }

  public List<FsFile> getFsFiles(int[] keys) throws IOException {
    var cachedArchive = index.getStorage().getCachedArchive();
    if (cachedArchive.getIndexId() == index.getId() && cachedArchive.getArchiveId() == archiveId) {
      return cachedArchive.getFiles();
    }
    var archiveFiles = getFiles(index.getStorage().loadArchive(this), keys);
    cachedArchive.set(index.getId(), archiveId, archiveFiles.getFiles());
    return cachedArchive.getFiles();
  }

  public FileData getFile(int id) {
    for (var file : files) {
      if (file.getId() != id) {
        continue;
      }
      return file;
    }
    return null;
  }

  public void updateFile(FileData file) {
    if (file.getId() == -1) {
      throw new IllegalArgumentException("File id is -1");
    }
    if (files == null) {
      files = new LinkedList<>();
    }
    var existingFile = getFile(file.getId());
    if (existingFile == null) {
      var newFile = new FileData();
      newFile.setId(file.getId());
      newFile.setNameHash(file.getNameHash());
      files.add(newFile);
      files.sort(Comparator.comparingInt(FileData::getId));
      return;
    }
    existingFile.setNameHash(file.getNameHash());
    existingFile.setContents(file.getContents());
  }

  public byte[] decompress(byte[] data) throws IOException {
    return decompress(data, null);
  }

  public byte[] decompress(byte[] data, int[] keys) throws IOException {
    if (data == null) {
      throw new IOException("Archive is null for " + index.getId() + "/" + archiveId);
    }
    var container = Container.decompress(data, keys);
    if (container == null) {
      throw new IOException("Unable to decrypt archive for " + index.getId() + "/" + archiveId);
    }
    var decompressedData = container.data;
    if (crc != container.crc) {
      throw new IOException(
          "CRC mismatch for " + index.getId() + "/" + archiveId + "; " + crc + "/" + container.crc);
    }
    if (container.revision != -1 && revision != container.revision) {
      setRevision(container.revision);
    }
    setCompression(container.compression);
    return decompressedData;
  }

  public ArchiveFiles getFiles(byte[] data) throws IOException {
    return getFiles(data, null);
  }

  public ArchiveFiles getFiles(byte[] data, int[] keys) throws IOException {
    var decompressedData = decompress(data, keys);
    var unpackedFiles = new ArchiveFiles();
    for (var fileEntry : files) {
      var file = new FsFile(fileEntry.getId());
      file.setNameHash(fileEntry.getNameHash());
      unpackedFiles.addFile(file);
    }
    unpackedFiles.loadContents(decompressedData);
    return unpackedFiles;
  }
}
