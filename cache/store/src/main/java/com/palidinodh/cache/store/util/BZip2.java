package com.palidinodh.cache.store.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

public class BZip2 {

  private static final byte[] BZIP_HEADER = new byte[] {'B', 'Z', 'h', '1'};

  private BZip2() {}

  public static byte[] compress(byte[] bytes) throws IOException {
    var is = new ByteArrayInputStream(bytes);
    var bout = new ByteArrayOutputStream();
    try (var os = new BZip2CompressorOutputStream(bout, 1)) {
      IOUtils.copy(is, os);
    }
    var out = bout.toByteArray();
    if (BZIP_HEADER[0] != out[0]
        || BZIP_HEADER[1] != out[1]
        || BZIP_HEADER[2] != out[2]
        || BZIP_HEADER[3] != out[3]) {
      throw new IOException("BZip compression failure");
    }
    return Arrays.copyOfRange(out, BZIP_HEADER.length, out.length);
  }

  public static byte[] decompress(byte[] bytes, int len) throws IOException {
    var data = new byte[len + BZIP_HEADER.length];
    System.arraycopy(BZIP_HEADER, 0, data, 0, BZIP_HEADER.length);
    System.arraycopy(bytes, 0, data, BZIP_HEADER.length, len);
    var os = new ByteArrayOutputStream();
    try (var is = new BZip2CompressorInputStream(new ByteArrayInputStream(data))) {
      IOUtils.copy(is, os);
    }
    return os.toByteArray();
  }
}
