package com.palidinodh.cache.store.util;

public final class RegionHash {

  private RegionHash() {}

  public static int findRegionId(int hash) {
    for (var i = 0; i < 65535; i++) {
      var name = (i >> 8) + "_" + (i & 255);
      var lHash = Djb2.hash("l" + name);
      var mHash = Djb2.hash("m" + name);
      if (hash != lHash && hash != mHash) {
        continue;
      }
      return i;
    }
    return -1;
  }
}
