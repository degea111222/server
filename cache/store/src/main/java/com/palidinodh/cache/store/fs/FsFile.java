package com.palidinodh.cache.store.fs;

import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class FsFile {

  private final int fileId;
  private int nameHash;
  private byte[] contents;

  public FsFile(int fileId) {
    this.fileId = fileId;
  }

  @Override
  public int hashCode() {
    var hash = 7;
    hash = 97 * hash + fileId;
    hash = 97 * hash + nameHash;
    hash = 97 * hash + Arrays.hashCode(contents);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (FsFile) obj;
    if (fileId != other.fileId) {
      return false;
    }
    if (nameHash != other.nameHash) {
      return false;
    }
    if (!Arrays.equals(contents, other.contents)) {
      return false;
    }
    return true;
  }

  public int getSize() {
    return contents.length;
  }
}
