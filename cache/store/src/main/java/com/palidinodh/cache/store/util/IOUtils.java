package com.palidinodh.cache.store.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class IOUtils {

  private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

  public static final int EOF = -1;

  private IOUtils() {}

  public static long copy(InputStream input, OutputStream output, int bufferSize)
      throws IOException {
    return copyLarge(input, output, new byte[bufferSize]);
  }

  public static int copy(InputStream input, OutputStream output) throws IOException {
    var count = copyLarge(input, output);
    if (count > Integer.MAX_VALUE) {
      return -1;
    }
    return (int) count;
  }

  public static long copyLarge(InputStream input, OutputStream output) throws IOException {
    return copy(input, output, DEFAULT_BUFFER_SIZE);
  }

  public static long copyLarge(InputStream input, OutputStream output, byte[] buffer)
      throws IOException {
    var count = 0L;
    int n;
    while (EOF != (n = input.read(buffer))) {
      output.write(buffer, 0, n);
      count += n;
    }
    return count;
  }
}
