package com.palidinodh.cache.definition.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DefinitionOption {

  private int index;
  private String text;

  public boolean equals(String text) {
    return this.text.equalsIgnoreCase(text);
  }

  public boolean equals(int index) {
    return this.index == index;
  }
}
