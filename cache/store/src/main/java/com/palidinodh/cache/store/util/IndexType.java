package com.palidinodh.cache.store.util;

public enum IndexType {
  ANIMATION_SKELETON,
  ANIMATION_SKIN,
  CONFIG,
  WIDGET,
  SOUND_EFFECT,
  MAP,
  MUSIC_1,
  MODEL,
  SPRITE,
  TEXTURE,
  BINARY,
  MUSIC_2,
  CLIENTSCRIPT,
  FONT,
  MIDI,
  INSTRUMENT,
  WORLD_MAP,
  UNKNOWN_17,
  UNKNOWN_18,
  UNKNOWN_19,
  UNKNOWN_20;

  public int getId() {
    return ordinal();
  }

  public static IndexType get(int index) {
    return index >= 0 && index <= values().length ? values()[index] : null;
  }

  public static IndexType get(String name) {
    name = name.toUpperCase();
    for (var indexType : values()) {
      if (!name.equals(indexType.name()) && !name.equals(indexType.name().replace("_", ""))) {
        continue;
      }
      return indexType;
    }
    return null;
  }
}
