package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.index.ArchiveData;
import com.palidinodh.cache.store.index.FileData;
import com.palidinodh.cache.store.index.IndexData;
import com.palidinodh.cache.store.util.Crc32;
import com.palidinodh.cache.store.util.Djb2;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.RegionHash;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Index {

  private final int id;
  private Storage storage;
  private final List<Archive> archives = new LinkedList<>();
  private int protocol = 6;
  private boolean named = true;
  private int revision;
  private int crc;
  private int compression;

  public Index(int id, Storage storage) {
    this.id = id;
    this.storage = storage;
  }

  @Override
  public int hashCode() {
    var hash = 3;
    hash = 97 * hash + id;
    hash = 97 * hash + revision;
    hash = 97 * hash + Objects.hashCode(archives);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (Index) obj;
    if (id != other.id) {
      return false;
    }
    if (revision != other.revision) {
      return false;
    }
    if (!Objects.equals(archives, other.archives)) {
      return false;
    }
    return true;
  }

  public int getHighestArchiveId() {
    archives.sort(
        (a1, a2) -> {
          return Integer.compare(a1.getArchiveId(), a2.getArchiveId());
        });
    return archives.isEmpty() ? 0 : archives.get(archives.size() - 1).getArchiveId();
  }

  public Archive addArchive(int id) {
    var archive = new Archive(this, id);
    archives.add(archive);
    return archive;
  }

  public Archive getArchive(int id) {
    for (var a : archives) {
      if (a.getArchiveId() != id) {
        continue;
      }
      return a;
    }
    return null;
  }

  public Archive findArchiveByName(String name) {
    var hash = Djb2.hash(name);
    for (var a : archives) {
      if (a.getNameHash() != hash) {
        continue;
      }
      return a;
    }
    return null;
  }

  public IndexData toIndexData() {
    archives.sort(
        (a1, a2) -> {
          return Integer.compare(a1.getArchiveId(), a2.getArchiveId());
        });
    var data = new IndexData();
    data.setProtocol(protocol);
    data.setRevision(revision);
    data.setNamed(named);
    var archiveDatas = new ArchiveData[archives.size()];
    data.setArchives(archiveDatas);
    var idx = 0;
    for (var archive : archives) {
      var ad = archiveDatas[idx++] = new ArchiveData();
      ad.setId(archive.getArchiveId());
      ad.setNameHash(archive.getNameHash());
      ad.setCrc(archive.getCrc());
      ad.setRevision(archive.getRevision());

      var files = archive.getFiles();
      ad.setFiles(files);
    }
    return data;
  }

  public boolean updateArchive(ArchiveData ad) throws IOException {
    var existingArchive = getArchive(ad.getId());
    var archive = existingArchive == null ? addArchive(ad.getId()) : existingArchive;
    ArchiveFiles files;
    var regionId = -1;
    try {
      if (existingArchive != null) {
        files = archive.getFiles(storage.loadArchive(archive));
      } else {
        files = new ArchiveFiles();
      }
      if (IndexType.get(id) == IndexType.MAP) {
        regionId =
            RegionHash.findRegionId(
                existingArchive != null ? existingArchive.getNameHash() : ad.getNameHash());
      }
    } catch (ZipException e) {
      if (IndexType.get(id) != IndexType.MAP) {
        throw e;
      }
      files = new ArchiveFiles();
      regionId =
          RegionHash.findRegionId(
              existingArchive != null ? existingArchive.getNameHash() : ad.getNameHash());
    }
    var foundChanges = ad.getNameHash() != 0 && ad.getNameHash() != archive.getNameHash();
    for (var file : ad.getFiles()) {
      var existingFile = files.getFile(file.getId());
      if (existingFile == null
          || Crc32.getHash(file.getContents()) != Crc32.getHash(existingFile.getContents())) {
        archive.updateFile(file);
        files.updateFile(file);
        foundChanges = true;
        continue;
      }
    }
    if (!foundChanges) {
      return false;
    }
    archive.setRevision(archive.getRevision() + 1);
    if (ad.getNameHash() != 0) {
      archive.setNameHash(ad.getNameHash());
    }
    if (existingArchive == null) {
      archive.setCompression(ad.getCompression());
    }
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    storage.saveArchive(archive, container.data);
    if (regionId != -1) {
      System.out.println("Region " + regionId + " no longer has xteas.");
    }
    return true;
  }

  public void updateDirectArchive(Archive ad, byte[] contents) throws IOException {
    var existingArchive = getArchive(ad.getArchiveId());
    var archive = existingArchive == null ? addArchive(ad.getArchiveId()) : existingArchive;
    var existingContents = existingArchive == null ? null : storage.loadArchive(archive);
    if (existingArchive != null
        && existingContents != null
        && (ad.getNameHash() == 0 || ad.getNameHash() == existingArchive.getNameHash())
        && Crc32.getHash(existingContents) == Crc32.getHash(contents)) {
      return;
    }
    archive.setRevision(archive.getRevision() + 1);
    if (ad.getNameHash() != 0) {
      archive.setNameHash(ad.getNameHash());
    }
    if (existingArchive == null) {
      archive.setCompression(ad.getCompression());
    }
    archive.setFiles(ad.getFiles());
    storage.saveArchive(archive, contents);
  }

  public void copyArchive(Archive ad, boolean skipExistingFiles) throws IOException {
    var existingArchive = getArchive(ad.getArchiveId());
    var archive = existingArchive == null ? addArchive(ad.getArchiveId()) : existingArchive;
    var adFiles = ad.getFiles(ad.getIndex().getStorage().loadArchive(archive));
    ArchiveFiles files;
    if (existingArchive != null) {
      files = archive.getFiles(storage.loadArchive(archive));
    } else {
      files = new ArchiveFiles();
    }
    var foundChanges = ad.getNameHash() != 0 && ad.getNameHash() != archive.getNameHash();
    for (var file : adFiles.getFiles()) {
      var existingFile = files.getFile(file.getFileId());
      if (existingFile == null
          || !skipExistingFiles
              && Crc32.getHash(file.getContents()) != Crc32.getHash(existingFile.getContents())) {
        var fileData = new FileData(file.getFileId(), file.getNameHash(), file.getContents());
        archive.updateFile(fileData);
        files.updateFile(fileData);
        foundChanges = true;
        continue;
      }
    }
    if (!foundChanges) {
      return;
    }
    archive.setRevision(archive.getRevision() + 1);
    if (ad.getNameHash() != 0) {
      archive.setNameHash(ad.getNameHash());
    }
    if (existingArchive == null) {
      archive.setCompression(ad.getCompression());
    }
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    var compressedData = container.data;
    storage.saveArchive(archive, compressedData);
  }

  public void copyDirectArchive(Archive ad, boolean skipIfExists) throws IOException {
    var existingArchive = getArchive(ad.getArchiveId());
    if (skipIfExists && existingArchive != null) {
      return;
    }
    var archive = existingArchive == null ? addArchive(ad.getArchiveId()) : existingArchive;
    var existingContents = existingArchive == null ? null : storage.loadArchive(archive);
    var contents = ad.getIndex().getStorage().loadArchive(ad);
    if (existingArchive != null
        && existingContents != null
        && (ad.getNameHash() == 0 || ad.getNameHash() == existingArchive.getNameHash())
        && Crc32.getHash(existingContents) == Crc32.getHash(contents)) {
      return;
    }
    archive.setRevision(archive.getRevision() + 1);
    if (ad.getNameHash() != 0) {
      archive.setNameHash(ad.getNameHash());
    }
    if (existingArchive == null) {
      archive.setCompression(ad.getCompression());
    }
    archive.setFiles(ad.getFiles());
    storage.saveArchive(archive, contents);
  }

  public void copyArchiveFiles(Archive ad, List<Integer> fileIds) throws IOException {
    var existingArchive = getArchive(ad.getArchiveId());
    var archive = existingArchive == null ? addArchive(ad.getArchiveId()) : existingArchive;
    var adFiles = ad.getFiles(ad.getIndex().getStorage().loadArchive(archive));
    ArchiveFiles files;
    if (existingArchive != null) {
      files = archive.getFiles(storage.loadArchive(archive));
    } else {
      files = new ArchiveFiles();
    }
    var foundChanges = ad.getNameHash() != 0 && ad.getNameHash() != archive.getNameHash();
    for (var file : adFiles.getFiles()) {
      var existingFile = files.getFile(file.getFileId());
      if (existingFile == null
          || fileIds.contains(file.getFileId())
              && Crc32.getHash(file.getContents()) != Crc32.getHash(existingFile.getContents())) {
        var fileData = new FileData(file.getFileId(), file.getNameHash(), file.getContents());
        archive.updateFile(fileData);
        files.updateFile(fileData);
        foundChanges = true;
        continue;
      }
    }
    if (!foundChanges) {
      return;
    }
    archive.setRevision(archive.getRevision() + 1);
    if (ad.getNameHash() != 0) {
      archive.setNameHash(ad.getNameHash());
    }
    if (existingArchive == null) {
      archive.setCompression(ad.getCompression());
    }
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    var compressedData = container.data;
    storage.saveArchive(archive, compressedData);
  }
}
