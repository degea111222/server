package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class ItemDefinition {

  private static final ItemDefinition DEFAULT = new ItemDefinition(-1);
  private static final String[] OPTIONS_ARRAY = {null, null, null, null, "Drop"};
  private static final List<DefinitionOption> OPTIONS_LIST =
      Arrays.asList(new DefinitionOption(4, "drop"));
  private static final String[] MAP_OPTIONS_ARRAY = {null, null, "Take", null, null};
  private static final List<DefinitionOption> MAP_OPTIONS_LIST =
      Arrays.asList(new DefinitionOption(2, "take"));
  private static final String[] EQUIP_OPTIONS_ARRAY = {
    "Remove", null, null, null, null, null, null, null, null
  };
  private static final List<DefinitionOption> EQUIP_OPTIONS_LIST =
      Arrays.asList(new DefinitionOption(0, "remove"));

  @Getter private static ItemDefinition[] definitions;

  @AllArgsConstructor
  @Getter
  public static enum Attribute {
    EQUIP_1(451),
    EQUIP_2(452),
    EQUIP_3(453),
    EQUIP_4(454),
    EQUIP_5(455),
    EQUIP_6(456),
    EQUIP_7(457),
    EQUIP_8(458);

    private int index;
  }

  private int id;
  private int notedId = -1;
  private int notedTemplateId = -1;
  private int inventoryModelId = -1;
  private String name = "null";
  private transient String lowerCaseName = "null";
  private int zoom2d = 2000;
  private int xan2d;
  private int yan2d;
  private int zan2d;
  private int xOffset2d;
  private int yOffset2d;
  private boolean stackable;
  private int value;
  private boolean members;
  private int maleModelId0 = -1;
  private int maleModelId1 = -1;
  private int maleModelId2 = -1;
  private int maleOffset;
  private int maleHeadModelId = -1;
  private int maleHeadModelId2 = -1;
  private int femaleModelId0 = -1;
  private int femaleModelId1 = -1;
  private int femaleModelId2 = -1;
  private int femaleOffset;
  private int femaleHeadModelId = -1;
  private int femaleHeadModelId2 = -1;
  private int[] colorToFind;
  private int[] colorToReplace;
  private int[] textureToFind;
  private int[] textureToReplace;
  private int shiftClickDropIndex = -2;
  private boolean tradeable;
  private int boughtId = -1;
  private int boughtTemplateId = -1;
  private int placeholderId = -1;
  private int placeholderTemplateId = -1;
  private int[] stackAmounts;
  private int[] stackIds;
  private int resizeX = 128;
  private int resizeY = 128;
  private int resizeZ = 128;
  private int ambient;
  private int contrast;
  private int team;
  private Map<Integer, Object> attributes;
  private String[] optionsArray = OPTIONS_ARRAY;
  private String[] mapOptionsArray = MAP_OPTIONS_ARRAY;
  private transient String[] equipOptionsArray = EQUIP_OPTIONS_ARRAY;
  private transient List<DefinitionOption> options = OPTIONS_LIST;
  private transient List<DefinitionOption> mapOptions = MAP_OPTIONS_LIST;
  private transient List<DefinitionOption> equipmentOptions = EQUIP_OPTIONS_LIST;

  public ItemDefinition(int id) {
    this.id = id;
  }

  public void readOpcodeValues(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      readValues(stream, opcode);
    }
    buildOptions();
  }

  private void readValues(Stream stream, int opcode) {
    switch (opcode) {
      case 1:
        inventoryModelId = stream.readUnsignedShort();
        break;
      case 2:
        name = stream.readString();
        lowerCaseName = name.toLowerCase();
        break;
      case 4:
        zoom2d = stream.readUnsignedShort();
        break;
      case 5:
        xan2d = stream.readUnsignedShort();
        break;
      case 6:
        yan2d = stream.readUnsignedShort();
        break;
      case 7:
        {
          xOffset2d = stream.readUnsignedShort();
          if (xOffset2d > 32767) {
            xOffset2d -= 65536;
          }
          break;
        }
      case 8:
        {
          yOffset2d = stream.readUnsignedShort();
          if (yOffset2d > 32767) {
            yOffset2d -= 65536;
          }
          break;
        }
      case 11:
        stackable = true;
        break;
      case 12:
        value = stream.readInt();
        break;
      case 16:
        members = true;
        break;
      case 23:
        {
          maleModelId0 = stream.readUnsignedShort();
          if (maleModelId0 == 65535) {
            maleModelId0 = -1;
          }
          maleOffset = stream.readUnsignedByte();
          break;
        }
      case 24:
        maleModelId1 = stream.readUnsignedShort();
        break;
      case 25:
        {
          femaleModelId0 = stream.readUnsignedShort();
          if (femaleModelId0 == 65535) {
            femaleModelId0 = -1;
          }
          femaleOffset = stream.readUnsignedByte();
          break;
        }
      case 26:
        femaleModelId1 = stream.readUnsignedShort();
        break;
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
        {
          if (mapOptionsArray.equals(MAP_OPTIONS_ARRAY)) {
            mapOptionsArray = Arrays.copyOf(mapOptionsArray, mapOptionsArray.length);
          }
          mapOptionsArray[opcode - 30] = stream.readString();
          if (mapOptionsArray[opcode - 30].equalsIgnoreCase("Hidden")) {
            mapOptionsArray[opcode - 30] = null;
          }
          break;
        }
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
        if (optionsArray.equals(OPTIONS_ARRAY)) {
          optionsArray = Arrays.copyOf(optionsArray, optionsArray.length);
        }
        optionsArray[opcode - 35] = stream.readString();
        break;
      case 40:
        {
          var length = stream.readUnsignedByte();
          colorToFind = new int[length];
          colorToReplace = new int[length];
          for (var i = 0; i < length; i++) {
            colorToFind[i] = stream.readUnsignedShort();
            colorToReplace[i] = stream.readUnsignedShort();
          }
          break;
        }
      case 41:
        {
          var length = stream.readUnsignedByte();
          textureToFind = new int[length];
          textureToReplace = new int[length];
          for (var i = 0; i < length; i++) {
            textureToFind[i] = stream.readUnsignedShort();
            textureToReplace[i] = stream.readUnsignedShort();
          }
          break;
        }
      case 42:
        shiftClickDropIndex = stream.readByte();
        break;
      case 65:
        tradeable = true;
        break;
      case 78:
        maleModelId2 = stream.readUnsignedShort();
        break;
      case 79:
        femaleModelId2 = stream.readUnsignedShort();
        break;
      case 90:
        maleHeadModelId = stream.readUnsignedShort();
        break;
      case 91:
        femaleHeadModelId = stream.readUnsignedShort();
        break;
      case 92:
        maleHeadModelId2 = stream.readUnsignedShort();
        break;
      case 93:
        femaleHeadModelId2 = stream.readUnsignedShort();
        break;
      case 95:
        zan2d = stream.readUnsignedShort();
        break;
      case 97:
        {
          notedId = stream.readUnsignedShort();
          if (notedId == 65535) {
            notedId = -1;
          }
          break;
        }
      case 98:
        notedTemplateId = stream.readUnsignedShort();
        break;
      case 100:
      case 101:
      case 102:
      case 103:
      case 104:
      case 105:
      case 106:
      case 107:
      case 108:
      case 109:
        {
          if (stackIds == null) {
            stackIds = new int[10];
            stackAmounts = new int[10];
          }
          stackIds[opcode - 100] = stream.readUnsignedShort();
          if (stackIds[opcode - 100] == 65535) {
            stackIds[opcode - 100] = -1;
          }
          stackAmounts[opcode - 100] = stream.readUnsignedShort();
          break;
        }
      case 110:
        resizeX = stream.readUnsignedShort();
        break;
      case 111:
        resizeY = stream.readUnsignedShort();
        break;
      case 112:
        resizeZ = stream.readUnsignedShort();
        break;
      case 113:
        ambient = stream.readByte();
        break;
      case 114:
        contrast = stream.readByte();
        break;
      case 115:
        team = stream.readUnsignedByte();
        break;
      case 139:
        boughtId = stream.readUnsignedShort();
        break;
      case 140:
        boughtTemplateId = stream.readUnsignedShort();
        break;
      case 148:
        placeholderId = stream.readUnsignedShort();
        break;
      case 149:
        placeholderTemplateId = stream.readUnsignedShort();
        break;
      case 249:
        attributes = stream.readScript();
        break;
      default:
        System.out.println("Item Definitions Unknown Opcode: " + opcode);
        break;
    }
  }

  private void buildOptions() {
    if (!optionsArray.equals(OPTIONS_ARRAY)) {
      options = new ArrayList<>();
      for (var i = 0; i < optionsArray.length; i++) {
        if (optionsArray[i] == null) {
          continue;
        }
        options.add(new DefinitionOption(i, optionsArray[i].toLowerCase()));
      }
      options = Collections.unmodifiableList(options);
    }
    if (!mapOptionsArray.equals(MAP_OPTIONS_ARRAY)) {
      mapOptions = new ArrayList<>();
      for (var i = 0; i < mapOptionsArray.length; i++) {
        if (mapOptionsArray[i] == null) {
          continue;
        }
        mapOptions.add(new DefinitionOption(i, mapOptionsArray[i].toLowerCase()));
      }
      mapOptions = Collections.unmodifiableList(mapOptions);
    }
    var equipOptions =
        new String[] {
          (String) getAttribute(Attribute.EQUIP_1),
          (String) getAttribute(Attribute.EQUIP_2),
          (String) getAttribute(Attribute.EQUIP_3),
          (String) getAttribute(Attribute.EQUIP_4),
          (String) getAttribute(Attribute.EQUIP_5),
          (String) getAttribute(Attribute.EQUIP_6),
          (String) getAttribute(Attribute.EQUIP_7),
          (String) getAttribute(Attribute.EQUIP_8)
        };
    var hasEquipOptions = false;
    for (var equipOption : equipOptions) {
      if (equipOption == null) {
        continue;
      }
      hasEquipOptions = true;
      break;
    }
    if (hasEquipOptions) {
      equipmentOptions = new ArrayList<>();
      equipmentOptions.add(new DefinitionOption(0, EQUIP_OPTIONS_ARRAY[0].toLowerCase()));
      for (var i = 0; i < equipOptions.length; i++) {
        if (equipOptions[i] == null) {
          continue;
        }
        equipmentOptions.add(new DefinitionOption(i + 1, equipOptions[i].toLowerCase()));
      }
      equipmentOptions = Collections.unmodifiableList(equipmentOptions);
    }
  }

  public int getNotedId() {
    return notedTemplateId == -1 ? notedId : -1;
  }

  public int getUnnotedId() {
    return notedTemplateId != -1 ? notedId : -1;
  }

  public boolean isUnnoted() {
    return notedTemplateId == -1;
  }

  public boolean isNoted() {
    return notedTemplateId != -1;
  }

  public int getPlaceholderId() {
    return placeholderTemplateId == -1 ? placeholderId : -1;
  }

  public int getUnplaceholderId() {
    return placeholderTemplateId != -1 ? placeholderId : -1;
  }

  public boolean isPlaceholder() {
    return placeholderTemplateId != -1;
  }

  public String getName() {
    if (getUnnotedId() != -1) {
      return getDefinition(getUnnotedId()).getName() + " (noted)";
    }
    if (placeholderTemplateId != -1 && placeholderId != -1) {
      return getDefinition(placeholderId).getName() + " (placeholder)";
    }
    return name;
  }

  public String getLowerCaseName() {
    if (getUnnotedId() != -1) {
      return getDefinition(getUnnotedId()).getLowerCaseName() + " (noted)";
    }
    if (placeholderTemplateId != -1 && placeholderId != -1) {
      return getDefinition(placeholderId).getLowerCaseName() + " (placeholder)";
    }
    return lowerCaseName;
  }

  public boolean isMembers() {
    if (getUnnotedId() != -1) {
      return getDefinition(getUnnotedId()).isMembers();
    }
    if (placeholderTemplateId != -1 && placeholderId != -1) {
      return getDefinition(placeholderId).isMembers();
    }
    return members;
  }

  public int getValue() {
    return getUnnotedId() != -1 ? getDefinition(getUnnotedId()).getValue() : value;
  }

  public boolean hasOption(String search) {
    if (options == null) {
      return false;
    }
    for (var option : options) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isOption(int index, String search) {
    var option = getOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getOption(int index) {
    if (options == null) {
      return null;
    }
    for (var option : options) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public boolean hasEquipmentOption(String search) {
    if (equipmentOptions == null) {
      return false;
    }
    for (var option : equipmentOptions) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isEquipmentOption(int index, String search) {
    var option = getEquipmentOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getEquipmentOption(int index) {
    if (equipmentOptions == null) {
      return null;
    }
    for (var option : equipmentOptions) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public Object getAttribute(Attribute attribute) {
    return attributes != null ? attributes.get(attribute.getIndex()) : null;
  }

  public static ItemDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static String getName(int id) {
    if (definitions == null) {
      return DEFAULT.getName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getName()
        : DEFAULT.getName();
  }

  public static String getLowerCaseName(int id) {
    if (definitions == null) {
      return DEFAULT.getLowerCaseName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getLowerCaseName()
        : DEFAULT.getLowerCaseName();
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.ITEM);
      definitions = new ItemDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new ItemDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.readOpcodeValues(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
