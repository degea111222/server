package com.palidinodh.cache.store.index;

import com.palidinodh.cache.store.util.Stream;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndexData {

  private int protocol;
  private int revision;
  private boolean named;
  private ArchiveData[] archives;

  public void load(byte[] data) {
    var stream = new Stream(data);
    protocol = stream.readUnsignedByte();
    if (protocol < 5 || protocol > 7) {
      throw new IllegalArgumentException("Unsupported protocol");
    }
    if (protocol >= 6) {
      revision = stream.readInt();
    }
    var hash = stream.readUnsignedByte();
    named = (1 & hash) != 0;
    if ((hash & ~1) != 0) {
      throw new IllegalArgumentException("Unknown flags");
    }
    var validArchivesCount = protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
    var lastArchiveId = 0;
    archives = new ArchiveData[validArchivesCount];
    for (var index = 0; index < validArchivesCount; ++index) {
      var archive =
          lastArchiveId += protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
      var ad = new ArchiveData();
      ad.setId(archive);
      archives[index] = ad;
    }
    if (named) {
      for (var index = 0; index < validArchivesCount; ++index) {
        var nameHash = stream.readInt();
        var ad = archives[index];
        ad.setNameHash(nameHash);
      }
    }
    for (var index = 0; index < validArchivesCount; ++index) {
      var crc = stream.readInt();
      var ad = archives[index];
      ad.setCrc(crc);
    }
    for (var index = 0; index < validArchivesCount; ++index) {
      var revision = stream.readInt();
      var ad = archives[index];
      ad.setRevision(revision);
    }
    var numberOfFiles = new int[validArchivesCount];
    for (var index = 0; index < validArchivesCount; ++index) {
      var num = protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
      numberOfFiles[index] = num;
    }
    List<FileData> files;
    for (var index = 0; index < validArchivesCount; ++index) {
      var ad = archives[index];
      var num = numberOfFiles[index];
      ad.setFiles(files = new LinkedList<>());
      var last = 0;
      for (var i = 0; i < num; ++i) {
        var fileId = last += protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
        var fd = new FileData();
        fd.setId(fileId);
        files.add(fd);
      }
    }
    if (named) {
      for (var index = 0; index < validArchivesCount; ++index) {
        var ad = archives[index];
        var num = numberOfFiles[index];
        files = ad.getFiles();
        for (var i = 0; i < num; ++i) {
          var fd = files.get(i);
          var name = stream.readInt();
          fd.setNameHash(name);
        }
      }
    }
  }

  public byte[] writeIndexData() {
    var stream = new Stream();
    stream.writeByte(protocol);
    if (protocol >= 6) {
      stream.writeInt(revision);
    }
    stream.writeByte(named ? 1 : 0);
    if (protocol >= 7) {
      stream.writeBigSmart(archives.length);
    } else {
      stream.writeShort(archives.length);
    }
    for (var i = 0; i < archives.length; ++i) {
      var a = archives[i];
      var archive = a.getId();
      if (i != 0) {
        var prev = archives[i - 1];
        archive -= prev.getId();
      }
      if (protocol >= 7) {
        stream.writeBigSmart(archive);
      } else {
        stream.writeShort(archive);
      }
    }
    if (named) {
      for (var i = 0; i < archives.length; ++i) {
        var a = archives[i];
        stream.writeInt(a.getNameHash());
      }
    }
    for (var i = 0; i < archives.length; ++i) {
      var a = archives[i];
      stream.writeInt(a.getCrc());
    }
    for (var i = 0; i < archives.length; ++i) {
      var a = archives[i];
      stream.writeInt(a.getRevision());
    }
    for (var i = 0; i < archives.length; ++i) {
      var a = archives[i];
      var len = a.getFiles().size();
      if (protocol >= 7) {
        stream.writeBigSmart(len);
      } else {
        stream.writeShort(len);
      }
    }
    for (var i = 0; i < archives.length; ++i) {
      var a = archives[i];
      for (var j = 0; j < a.getFiles().size(); ++j) {
        var file = a.getFiles().get(j);
        var offset = file.getId();
        if (j != 0) {
          var prev = a.getFiles().get(j - 1);
          offset -= prev.getId();
        }
        if (protocol >= 7) {
          stream.writeBigSmart(offset);
        } else {
          stream.writeShort(offset);
        }
      }
    }
    if (named) {
      for (var i = 0; i < archives.length; ++i) {
        var a = archives[i];
        for (var j = 0; j < a.getFiles().size(); ++j) {
          var file = a.getFiles().get(j);
          stream.writeInt(file.getNameHash());
        }
      }
    }
    return stream.toByteArray();
  }
}
