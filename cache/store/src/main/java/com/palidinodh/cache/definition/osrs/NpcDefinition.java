package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.Getter;

@Getter
public class NpcDefinition {

  private static final NpcDefinition DEFAULT = new NpcDefinition(-1);

  @Getter private static NpcDefinition[] definitions;

  private int id;
  private String name = "null";
  private transient String lowerCaseName = "null";
  private int[] modelIds;
  private int size = 1;
  private int combatLevel = -1;
  private int standAnimation = -1;
  private int walkAnimation = -1;
  private int turn180Animation = -1;
  private int turn90RightAnimation = -1;
  private int turn90LeftAnimation = -1;
  private int[] colorToFind;
  private int[] colorToReplace;
  private int[] textureToFind;
  private int[] textureToReplace;
  private int[] chatheadModelIds;
  private boolean minimapVisible = true;
  private int widthScale = 128;
  private int heightScale = 128;
  private boolean renderPriority;
  private int ambient;
  private int contrast;
  private int headIcon = -1;
  private int rotationSpeed = 32;
  private int varbitId = -1;
  private int varpId = -1;
  private boolean interactable = true;
  private boolean rotationFlag = true;
  private boolean pet;
  private int[] configs;
  private Map<Integer, Object> attributes;
  private String[] optionsArray;
  private transient List<DefinitionOption> options;

  public NpcDefinition(int id) {
    this.id = id;
  }

  public void readOpcodeValues(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      readValues(stream, opcode);
    }
    buildOptions();
  }

  private void readValues(Stream stream, int opcode) {
    switch (opcode) {
      case 1:
        {
          modelIds = new int[stream.readUnsignedByte()];
          for (var i = 0; i < modelIds.length; i++) {
            var modelId = stream.readUnsignedShort();
            if (modelId == 65535) {
              modelId = -1;
            }
            modelIds[i] = modelId;
          }
          break;
        }
      case 2:
        name = stream.readString();
        lowerCaseName = name.toLowerCase();
        break;
      case 12:
        size = stream.readUnsignedByte();
        break;
      case 13:
        standAnimation = stream.readUnsignedShort();
        break;
      case 14:
        walkAnimation = stream.readUnsignedShort();
        break;
      case 15:
        turn90LeftAnimation = stream.readUnsignedShort();
        break;
      case 16:
        turn90RightAnimation = stream.readUnsignedShort();
        break;
      case 17:
        walkAnimation = stream.readUnsignedShort();
        turn180Animation = stream.readUnsignedShort();
        turn90RightAnimation = stream.readUnsignedShort();
        turn90LeftAnimation = stream.readUnsignedShort();
        break;
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
        {
          if (optionsArray == null) {
            optionsArray = new String[5];
          }
          optionsArray[opcode - 30] = stream.readString();
          if (optionsArray[opcode - 30].equalsIgnoreCase("Hidden")) {
            optionsArray[opcode - 30] = null;
          }
          break;
        }
      case 40:
        {
          var length = stream.readUnsignedByte();
          colorToFind = new int[length];
          colorToReplace = new int[length];
          for (var i = 0; i < length; i++) {
            colorToFind[i] = stream.readUnsignedShort();
            colorToReplace[i] = stream.readUnsignedShort();
          }
          break;
        }
      case 41:
        {
          var length = stream.readUnsignedByte();
          textureToFind = new int[length];
          textureToReplace = new int[length];
          for (var i = 0; i < length; i++) {
            textureToFind[i] = stream.readUnsignedShort();
            textureToReplace[i] = stream.readUnsignedShort();
          }
          break;
        }
      case 60:
        {
          chatheadModelIds = new int[stream.readUnsignedByte()];
          for (var i = 0; i < chatheadModelIds.length; i++) {
            var modelId = stream.readUnsignedShort();
            if (modelId == 65535) {
              modelId = -1;
            }
            chatheadModelIds[i] = modelId;
          }
          break;
        }
      case 93:
        minimapVisible = false;
        break;
      case 95:
        combatLevel = stream.readUnsignedShort();
        break;
      case 97:
        widthScale = stream.readUnsignedShort();
        break;
      case 98:
        heightScale = stream.readUnsignedShort();
        break;
      case 99:
        renderPriority = true;
        break;
      case 100:
        ambient = stream.readByte();
        break;
      case 101:
        contrast = stream.readByte();
        break;
      case 102:
        headIcon = stream.readUnsignedShort();
        break;
      case 103:
        rotationSpeed = stream.readUnsignedShort();
        break;
      case 107:
        interactable = false;
        break;
      case 109:
        rotationFlag = false;
        break;
      case 111:
        pet = true;
        break;
      case 106:
      case 118:
        {
          varbitId = stream.readUnsignedShort();
          if (varbitId == 65535) {
            varbitId = -1;
          }
          varpId = stream.readUnsignedShort();
          if (varpId == 65535) {
            varpId = -1;
          }
          var endVar = -1;
          if (opcode == 118) {
            endVar = stream.readUnsignedShort();
            if (endVar == 65535) {
              endVar = -1;
            }
          }
          var length = stream.readUnsignedByte();
          configs = new int[length + 2];
          for (var i = 0; i <= length; i++) {
            configs[i] = stream.readUnsignedShort();
            if (configs[i] == 65535) {
              configs[i] = -1;
            }
          }
          configs[length + 1] = endVar;
          break;
        }
      case 249:
        attributes = stream.readScript();
        break;
      default:
        System.out.println("Npc Definitions Unknown Opcode: " + opcode);
        break;
    }
  }

  private void buildOptions() {
    if (optionsArray != null) {
      options = new ArrayList<>();
      for (var i = 0; i < optionsArray.length; i++) {
        if (optionsArray[i] == null) {
          continue;
        }
        options.add(new DefinitionOption(i, optionsArray[i].toLowerCase()));
      }
      options = Collections.unmodifiableList(options);
    }
  }

  public int[] getStanceAnimations() {
    int turn180Animation = this.turn180Animation;
    int turn90RightAnimation = this.turn90RightAnimation;
    int turn90LeftAnimation = this.turn90LeftAnimation;
    if (turn180Animation == -1) {
      turn180Animation = walkAnimation;
    }
    if (turn90RightAnimation == -1) {
      turn90RightAnimation = walkAnimation;
    }
    if (turn90LeftAnimation == -1) {
      turn90LeftAnimation = walkAnimation;
    }
    return new int[] {
      standAnimation,
      standAnimation,
      walkAnimation,
      turn180Animation,
      turn90RightAnimation,
      turn90LeftAnimation,
      walkAnimation
    };
  }

  public boolean hasOptions() {
    return options != null;
  }

  public boolean hasOption(String search) {
    if (options == null) {
      return false;
    }
    for (var option : options) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isOption(int index, String search) {
    var option = getOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getOption(int index) {
    if (options == null) {
      return null;
    }
    for (var option : options) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public static NpcDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static String getName(int id) {
    if (definitions == null) {
      return DEFAULT.getName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getName()
        : DEFAULT.getName();
  }

  public static String getLowerCaseName(int id) {
    if (definitions == null) {
      return DEFAULT.getLowerCaseName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getLowerCaseName()
        : DEFAULT.getLowerCaseName();
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.NPC);
      definitions = new NpcDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new NpcDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.readOpcodeValues(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
