package com.palidinodh.cache.store.util;

public enum ConfigType {
  UNKNOWN_0,
  UNDERLAY,
  UNKNOWN_2,
  IDENTITY_KIT,
  OVERLAY,
  INVENTORY,
  OBJECT,
  UNKNOWN_7,
  ENUM,
  NPC,
  ITEM,
  PARAM,
  ANIMATION, // SEQUENCE
  GRAPHIC, // SPOTANIM
  VARBIT,
  VAR_CLIENTSTRING,
  VAR_PLAYER,
  UNKNOWN_17,
  UNKNOWN_18,
  VAR_CLIENT,
  UNKNOWN_20,
  UNKNOWN_21,
  UNKNOWN_22,
  UNKNOWN_23,
  UNKNOWN_24,
  UNKNOWN_25,
  UNKNOWN_26,
  UNKNOWN_27,
  UNKNOWN_28,
  UNKNOWN_29,
  UNKNOWN_30,
  UNKNOWN_31,
  HIT_MARK,
  HITPOINTS_BAR,
  STRUCT,
  AREA;

  public int getId() {
    return ordinal();
  }

  public static ConfigType get(int index) {
    return index >= 0 && index < values().length ? values()[index] : null;
  }

  public static ConfigType get(String name) {
    name = name.toUpperCase();
    for (var configType : values()) {
      if (!name.equals(configType.name()) && !name.equals(configType.name().replace("_", ""))) {
        continue;
      }
      return configType;
    }
    return null;
  }
}
