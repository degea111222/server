# BattleScape

This is the open side of the BattleScape RSPS. Due to the private core, to fully test any changes, you'll need to request access to the beta server were you can freely deploy your changes as often as needed.

## Getting Started

These instructions will get you a copy of the project up on your local machine for development purposes.

### Prerequisites

- [IntelliJ IDEA](https://www.jetbrains.com/idea/) is recommended, but Eclipse or Visual Studio Code can also be used without any issues.
- [Git](https://git-scm.com), needed to clone the repo and also to pull and push any changes.
- [Git LFS](https://git-lfs.github.com), needed to manage large files such as the cache.
- [Java 11 Development Kit](https://www.oracle.com/java/technologies/javase-downloads.html).

Required extensions:
- Lombok
- Java Extension Pack (Visual Studio Code only)

Creating your own fork:
- Select `Fork` in the top-right of this page
- Once it finishes, you'll be taken to your forked copy of our repo
- Select `Clone or download` and copy the url, you'll need this to start installing

### Installing

- Install all of the software listed in prerequisites
- Create your own fork, as described in prerequisites
- Open the command prompt, terminal, etc, depending on your OS
- Type in `git lfs install`
- `cd ...` to the location you'd like to create the BattleScape directory at
- Type in `git clone URL_YOU_COPIED_EARLIER BattleScape`
- Open IntelliJ
- Select `open or import`
- Select your BattleScape directory
- Select `add as maven project` in the popup about an unmanaged pom.xml
- Go to `File` -> `Project Structure` -> `Project SDK` -> select Java 11

## Running/Debugging

Autocompletion and error checking against the core will work, but you won't be able to deploy a test server on your local machine. You can request access to the beta server where you can upload your code to test and debug them.

There is an ongoing effort to port a large amount of the closed repo over to the open repo to provide better access to what can be worked on, but there are currently many limitations to what can be changed due to what is publicly available. The reason for the closed repo is to limit access to some core classes to prevent anyone from simply creating their own server from BattleScape instead of contributing to it.

## Getting the Latest Changes

Your forked repo won't automatically fetch or pull updates made to the official repo.

- Open the command prompt
- `cd ...` into your BattleScape directory
- Type in `git remote add Official https://gitlab.com/battlescape/server.git`

#### IntelliJ
- `VCS` -> `Git` -> `Pull...` -> change origin to Official -> `Pull`

#### Visual Studio Code
- `Source Control` -> `More Actions...` -> `Pull from...` -> `Official` -> `Official/master`

## Contributing

First, make sure you've configured your username and email for git
- Open the command prompt
- Type in `git config --global user.name "YOUR_USERNAME"`
- Type in `git config --global user.email "YOUR_EMAIL"`

Once you've completed change(s), use the appropriate commit and push options in your editor to add the changes to your forked repo.

To request your changes to be added to the official repo, visit your forked repo on GitLab. Select `New pull request`, and then submit your changes from there.
