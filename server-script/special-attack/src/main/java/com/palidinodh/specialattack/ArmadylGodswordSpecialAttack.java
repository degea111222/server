package com.palidinodh.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ARMADYL_GODSWORD,
  ItemId.ARMADYL_GODSWORD_20593,
  ItemId.ARMADYL_GODSWORD_BEGINNER_32326,
  ItemId.ARMADYL_GODSWORD_OR
})
class ArmadylGodswordSpecialAttack extends SpecialAttack {

  ArmadylGodswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(7644);
    entry.castGraphic(new Graphic(1211));
    entry.castSound(new Sound(2850));
    entry.accuracyModifier(2);
    entry.damageModifier(1.1);
    addEntry(entry);
  }
}
