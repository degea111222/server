package com.palidinodh.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.VOLATILE_NIGHTMARE_STAFF)
class VolatileNightmareStaffSpecialAttack extends SpecialAttack {

  VolatileNightmareStaffSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(55);
    entry.animation(8532);
    entry.castGraphic(new Graphic(1760, 0, 50));
    entry.impactGraphic(new Graphic(1759));
    entry.accuracyModifier(1.5);
    entry.magic(true);
    entry.magicDamage(44);
    entry.magicLevelScale(75);
    entry.magicLevelScaleDivider(1.5);
    addEntry(entry);
  }
}
