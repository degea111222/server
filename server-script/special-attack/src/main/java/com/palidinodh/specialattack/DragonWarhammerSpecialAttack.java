package com.palidinodh.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DRAGON_WARHAMMER, ItemId.DRAGON_WARHAMMER_20785})
class DragonWarhammerSpecialAttack extends SpecialAttack {

  DragonWarhammerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(1378);
    entry.castGraphic(new Graphic(1292));
    entry.castSound(new Sound(2520));
    entry.damageModifier(1.5);
    addEntry(entry);
  }
}
