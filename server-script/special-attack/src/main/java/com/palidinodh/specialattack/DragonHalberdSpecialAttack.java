package com.palidinodh.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DRAGON_HALBERD)
class DragonHalberdSpecialAttack extends SpecialAttack {

  DragonHalberdSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(30);
    entry.animation(1203);
    entry.impactGraphic(new Graphic(1172, 100));
    entry.castSound(new Sound(2533));
    entry.damageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void attackTickHook(AttackTickHooks hooks) {
    var opponent = hooks.getOpponent();
    if (opponent.getSizeX() > 1 || opponent.isNpc() && opponent.getId() == NpcId.COMBAT_DUMMY) {
      hooks.setDoubleHit(true);
    }
  }
}
