package com.palidinodh.specialattack;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BARRELCHEST_ANCHOR)
class BarrelchestAnchorSpecialAttack extends SpecialAttack {

  BarrelchestAnchorSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(5870);
    entry.castGraphic(new Graphic(1027));
    entry.castSound(new Sound(3481));
    entry.accuracyModifier(2.0);
    entry.damageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (!hooks.getOpponent().isPlayer()) {
      return;
    }
    var skillId = PRandom.arrayRandom(Skills.ATTACK, Skills.DEFENCE, Skills.RANGED, Skills.MAGIC);
    hooks
        .getOpponent()
        .asPlayer()
        .getSkills()
        .changeStat(skillId, (int) -(hooks.getDamage() * 0.1));
  }
}
