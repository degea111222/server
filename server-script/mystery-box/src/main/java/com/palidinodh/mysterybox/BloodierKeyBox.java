package com.palidinodh.mysterybox;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.BLOODIER_KEY_32305)
class BloodierKeyBox extends MysteryBox {

  private static List<RandomItem> weightless =
      RandomItem.weightless(
          RandomItem.combine(
              RandomItem.buildList(
                  new RandomItem(ItemId.COINS, 1_000_000),
                  new RandomItem(ItemId.COINS, 10_000_000)),
              ItemTables.VERY_RARE,
              ItemTables.RARE,
              ItemTables.UNCOMMON,
              ItemTables.COMMON,
              ItemTables.BARROWS_PIECES,
              RandomItem.buildList(
                  new RandomItem(ItemId.CLUE_SCROLL_MASTER),
                  new RandomItem(ItemId.CLUE_SCROLL_ELITE),
                  new RandomItem(ItemId.CLUE_SCROLL_HARD),
                  new RandomItem(ItemId.CLUE_SCROLL_MEDIUM))));
  private static List<RandomItem> baseTable =
      RandomItem.combine(ItemTables.UNCOMMON, ItemTables.COMMON);

  @Override
  public Item getRandomItem(Player player) {
    if (player.isGameModeIronmanRelated()) {
      return PRandom.randomE(32) == 0
          ? RandomItem.getItem(ItemTables.BARROWS_PIECES)
          : new RandomItem(ItemId.COINS, 200_000, 2_000_000).getItem();
    }
    if (PRandom.randomE(128) == 0) {
      return PRandom.randomE(32) == 0
          ? RandomItem.getItem(ItemTables.VERY_RARE)
          : new RandomItem(ItemId.COINS, 8_000_000, 80_000_000).getItem();
    } else if (PRandom.randomE(32) == 0) {
      int type = PRandom.randomE(4);
      if (type == 0) {
        return new Item(ItemId.CLUE_SCROLL_MASTER);
      } else if (type == 1) {
        return new Item(ItemId.CLUE_SCROLL_ELITE);
      } else if (type == 2) {
        return new Item(ItemId.CLUE_SCROLL_HARD);
      } else if (type == 3) {
        return new Item(ItemId.CLUE_SCROLL_MEDIUM);
      }
      return new Item(ItemId.CLUE_SCROLL_EASY);
    } else if (PRandom.randomE(8) == 0) {
      return PRandom.randomE(32) == 0
          ? RandomItem.getItem(ItemTables.RARE)
          : new RandomItem(ItemId.COINS, 2_000_000, 20_000_000).getItem();
    }
    return PRandom.randomE(32) == 0
        ? RandomItem.getItem(baseTable)
        : new RandomItem(ItemId.COINS, 1_000_000, 10_000_000).getItem();
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    if (player.isGameModeIronmanRelated()) {
      var coins =
          RandomItem.buildList(
              new RandomItem(ItemId.COINS, 200_000), new RandomItem(ItemId.COINS, 2_000_000));
      return RandomItem.weightless(RandomItem.combine(coins, ItemTables.BARROWS_PIECES));
    }
    return weightless;
  }
}
