package com.palidinodh.worldevent.pvptournament;

import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ModeMapRoom {

  private Tile tile1;
  private Tile tile2;
}
