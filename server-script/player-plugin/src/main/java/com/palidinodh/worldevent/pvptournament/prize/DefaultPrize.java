package com.palidinodh.worldevent.pvptournament.prize;

import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PCollection;
import com.palidinodh.worldevent.holidayboss.HolidayBossEvent;
import java.util.List;

public class DefaultPrize implements Prize {

  private boolean rewardBonds;

  public DefaultPrize(boolean rewardBonds) {
    this.rewardBonds = rewardBonds;
  }

  @Override
  public List<Item> getItems(int position) {
    var holidayItemId = Main.getWorld().getWorldEvent(HolidayBossEvent.class).getItemId();
    List<Item> items = null;
    switch (position) {
      case 0:
        items = PCollection.toList(new Item(ItemId.COINS, 5_000_000), new Item(holidayItemId, 12));
        if (rewardBonds) {
          items.add(new Item(ItemId.BOND_32318, 50));
        }
        break;
      case 1:
        items = PCollection.toList(new Item(ItemId.COINS, 500_000), new Item(holidayItemId, 8));
        if (rewardBonds) {
          items.add(new Item(ItemId.BOND_32318, 15));
        }
        break;
      case 2:
        items = PCollection.toList(new Item(ItemId.COINS, 50_000), new Item(holidayItemId, 4));
        if (rewardBonds) {
          items.add(new Item(ItemId.BOND_32318, 10));
        }
        break;
    }
    return items;
  }

  @Override
  public String getMessage() {
    return "Prizes include 5M" + (rewardBonds ? " and 75 bonds" : "") + ".";
  }
}
