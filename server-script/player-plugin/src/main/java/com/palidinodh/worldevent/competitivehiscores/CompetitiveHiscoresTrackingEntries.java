package com.palidinodh.worldevent.competitivehiscores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;

@Getter
class CompetitiveHiscoresTrackingEntries {

  private Map<Integer, CompetitiveHiscoresIndividual> individuals = new HashMap<>();
  private Map<Integer, CompetitiveHiscoresClan> clans = new HashMap<>();

  public List<CompetitiveHiscoresIndividual> getIndividualsAsList() {
    return new ArrayList<>(individuals.values());
  }

  public List<CompetitiveHiscoresClan> getClansAsList() {
    return new ArrayList<>(clans.values());
  }
}
