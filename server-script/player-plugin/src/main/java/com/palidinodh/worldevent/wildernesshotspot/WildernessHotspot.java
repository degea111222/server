package com.palidinodh.worldevent.wildernesshotspot;

import com.palidinodh.osrscore.model.tile.Tile;

interface WildernessHotspot {

  String getName();

  default boolean isLowFrequency() {
    return false;
  }

  boolean inside(Tile tile);
}
