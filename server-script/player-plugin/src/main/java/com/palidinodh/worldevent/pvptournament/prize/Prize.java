package com.palidinodh.worldevent.pvptournament.prize;

import com.palidinodh.osrscore.model.item.Item;
import java.util.List;

public interface Prize {

  default List<Item> getItems(int position) {
    return null;
  }

  default String getMessage() {
    return null;
  }

  default boolean addItem(int position, Item item) {
    return false;
  }
}
