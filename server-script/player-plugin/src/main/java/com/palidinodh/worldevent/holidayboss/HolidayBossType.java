package com.palidinodh.worldevent.holidayboss;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HolidayBossType {
  ANTI_SANTA(
      NpcId.ANTI_SANTA_16019,
      0,
      new Tile(2269, 4062, 4),
      new Tile(2272, 4045, 4),
      ItemId.SNOWBALL_TOKEN_32339);

  private final int npcId;
  private final int npcMoveDistance;
  private final Tile npcTile;
  private final Tile teleportTile;
  private final int itemId;
}
