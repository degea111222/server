package com.palidinodh.worldevent.pvptournament.state;

import com.palidinodh.osrscore.Main;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;
import com.palidinodh.worldevent.pvptournament.prize.DefaultPrize;
import lombok.Getter;

public class IdleState implements State {

  private PvpTournamentEvent tournament;
  @Getter private boolean announcedStartingSoon;

  public IdleState(PvpTournamentEvent tournament) {
    this.tournament = tournament;
    tournament.setMode(null);
    tournament.setPrize(new DefaultPrize(false));
    tournament.getPlayers().clear();
    tournament.setController(null);
  }

  @Override
  public String getMessage() {
    if (PvpTournamentEvent.TIME.length == 0 || !Main.getWorld().isPrimary()) {
      return "N/A";
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = PvpTournamentEvent.getNextTime();
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  @Override
  public int getTime() {
    return 0;
  }

  @Override
  public void execute() {
    var nextTime = PvpTournamentEvent.getNextTime();
    if (nextTime == null) {
      return;
    }
    var isCustom = !(tournament.getPrize() instanceof DefaultPrize);
    var isCustomReady = isCustom && tournament.getMode() != null;
    if (!isCustom && !Main.getWorld().isPrimary()) {
      return;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (!isCustom && !announcedStartingSoon) {
      if (remainingMinutes == 5) {
        announcedStartingSoon = true;
        Main.getWorld()
            .sendNotice(
                "The tournament lobby will open in "
                    + remainingMinutes
                    + " minutes! Use the combat room portal to get their quickly.");
      }
    }
    if (isCustom ? !isCustomReady : remainingMinutes != 0) {
      return;
    }
    tournament.setState(new LobbyState(tournament));
  }
}
