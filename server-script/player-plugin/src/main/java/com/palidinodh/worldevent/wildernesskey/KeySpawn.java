package com.palidinodh.worldevent.wildernesskey;

import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum KeySpawn {
  FROZEN_WASTE_PLATEAU_RUNE_ROCKS_1(
      new Tile(2945, 3914), new Tile(2947, 3903), "at the Frozen Waste Plateau"),
  FROZEN_WASTE_PLATEAU_RUNE_ROCKS_2(
      new Tile(2966, 3935), new Tile(2947, 3903), "at the Frozen Waste Plateau"),
  FROZEN_WASTE_PLATEAU_RUNE_ROCKS_3(
      new Tile(2979, 3939), new Tile(2947, 3903), "at the Frozen Waste Plateau"),
  AGILITY_COURSE_ENTRANCE(
      new Tile(2998, 3914), new Tile(2947, 3903), "at the Agility Training Area"),
  DEEP_WILD_DUNGEON_ENTRANCE(
      new Tile(3048, 3925), new Tile(2539, 4715), "near the Deep Wild Dungeon"),
  PIRATES_HIDEOUT(new Tile(3041, 3952), new Tile(2539, 4715), "at the Pirates' Hideout"),

  DEEP_WILD_MINE(new Tile(3056, 3943), new Tile(2539, 4715), "at the Deep Wild Mine"),
  DEEP_WILD_ANVIL(new Tile(3062, 3951), new Tile(2539, 4715), "at the Deep Wild Anvil"),
  BELOW_MAGE_ARENA(new Tile(3106, 3908), new Tile(2539, 4715), "south of the Mage Arena"),
  MAGE_ARENA_LEVER(new Tile(3094, 3956), new Tile(2539, 4715), "at the Mage Arena Lever"),
  DEEP_WILD_LEVER(new Tile(3157, 3923), new Tile(2539, 4715), "at the Deep Wild Lever"),

  MAGIC_AXE_HUT(new Tile(3188, 3960), new Tile(3153, 3923), "at the Magic Axe Hut"),
  RESOURCE_AREA(new Tile(3190, 3932), new Tile(3153, 3923), "at the Resource Area"),
  SCORPION_PIT(new Tile(3238, 3939), new Tile(3153, 3923), "at the Scorpion Pit"),
  SCOPRIA(new Tile(3240, 10340), new Tile(3153, 3923), "beneath the Scorpion Pit"),
  ROGUES_CASTLE(new Tile(3282, 3930), new Tile(3153, 3923), "at the Rogues' Castle"),
  CHAOS_ELEMENTAL(new Tile(3267, 3926), new Tile(3153, 3923), "near the Chaos Elemental"),

  OBELISK_50(new Tile(3312, 3922), new Tile(3336, 3895), "near the Obelisk in Level 50"),
  LAVA_FORGE_1(new Tile(3367, 3935), new Tile(3336, 3895), "at the Lava Forge"),
  LAVA_FORGE_2(new Tile(3362, 3936), new Tile(3336, 3895), "at the Lava Forge"),
  LAVA_FORGE_3(new Tile(3372, 3936), new Tile(3336, 3895), "at the Lava Forge");

  private final Tile keyTile;
  private final Tile teleportTile;
  private final String name;

  public String getShortName() {
    var shortName = name;
    var indexOfThe = name.indexOf("the ");
    if (indexOfThe != -1) {
      shortName = shortName.substring(indexOfThe + 4);
    }
    shortName = shortName.replace("Wild", "").replace("Wilderness", "");
    return shortName;
  }
}
