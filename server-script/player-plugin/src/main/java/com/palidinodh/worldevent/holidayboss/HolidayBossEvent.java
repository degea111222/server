package com.palidinodh.worldevent.holidayboss;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PTime;
import lombok.Getter;

public class HolidayBossEvent extends PEvent {

  private static final boolean ENABLED = true;
  private static final int SOON_MINUTES = 5;
  private static final String[] TIME = {
    "1:15", "3:15", "5:15", "7:15", "9:15", "11:15", "13:15", "15:15", "17:15", "19:15", "21:15",
    "23:15"
  };
  private static final int[] HOURS;
  private static final int[] MINUTES;

  @Inject private transient World world;
  @Getter private transient HolidayBossType type;
  private transient Npc boss;

  public HolidayBossEvent() {
    super(4);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("holiday_boss_name")) {
      return type != null ? NpcDefinition.getName(type.getNpcId()) : "Holiday Boss";
    }
    if (name.equals("holiday_boss_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (boss != null && !boss.isVisible()) {
      boss = null;
    }
    if (!canRun()) {
      return;
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      world.sendNotice(
          "<col=ff0000>"
              + NpcDefinition.getName(type.getNpcId())
              + " will spawn in "
              + SOON_MINUTES
              + " minutes! Use ::holidayboss to teleport there!");
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  public void setType(HolidayBossType type) {
    this.type = type;
    Main.setHolidayToken(type != null ? type.getItemId() : -1);
  }

  public int getItemId() {
    return type != null ? type.getItemId() : -1;
  }

  public void teleport(Player player) {
    if (!canRun()) {
      player.getGameEncoder().sendMessage("There are no active holiday events at this time.");
      return;
    }
    player.getMovement().teleport(type.getTeleportTile());
  }

  private void startEvent() {
    if (!canRun()) {
      return;
    }
    boss = world.addNpc(new NpcSpawn(type.getNpcTile(), type.getNpcId()));
    boss.getSpawn().moveDistance(type.getNpcMoveDistance());
    world.sendBroadcast(
        NpcDefinition.getName(boss.getId()) + " has spawned! Use ::holidayboss to teleport there!");
  }

  private String getNextTimeText() {
    if (boss != null) {
      return "Spawned";
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    for (int i = 0; i < HOURS.length; i++) {
      int hour = HOURS[i];
      int minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  private boolean canRun() {
    if (!ENABLED) {
      return false;
    }
    if (type == null) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (int i = 0; i < TIME.length; i++) {
        String[] data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }
}
