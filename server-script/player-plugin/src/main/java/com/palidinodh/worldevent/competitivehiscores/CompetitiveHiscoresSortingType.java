package com.palidinodh.worldevent.competitivehiscores;

public enum CompetitiveHiscoresSortingType {
  INDIVIDUALS,
  FRIENDS,
  CLANS,
  SELF
}
