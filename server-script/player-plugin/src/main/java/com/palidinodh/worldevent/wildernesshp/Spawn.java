package com.palidinodh.worldevent.wildernesshp;

import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum Spawn {
  VARROCK(new Tile(3235, 3539), new Tile(3243, 3517), "Varrock Wilderness"),
  HILL_GIANT_HUT(new Tile(3310, 3660), new Tile(3296, 3650), "Hill Giant Hut"),
  DARK_WARRIORS_FORTRESS(new Tile(3037, 3611), new Tile(3017, 3592), "Dark Warriors' Fortress"),
  DEMONIC_RUINS(new Tile(3277, 3872), new Tile(3253, 3852), "Demonic Ruins");

  private final Tile npcTile;
  private final Tile teleportTile;
  private final String location;
}
