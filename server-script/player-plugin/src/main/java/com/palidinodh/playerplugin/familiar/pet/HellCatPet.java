package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class HellCatPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.HELL_CAT, NpcId.HELLCAT_6668, NpcId.HELLCAT));
    builder.entry(new Pet.Entry(ItemId.WILY_HELLCAT, NpcId.WILY_HELLCAT_6696, NpcId.WILY_HELLCAT));
    return builder;
  }
}
