package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class KrakenPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_KRAKEN, NpcId.KRAKEN_6656, NpcId.KRAKEN));
    return builder;
  }
}
