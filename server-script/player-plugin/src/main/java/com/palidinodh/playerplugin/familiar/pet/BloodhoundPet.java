package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class BloodhoundPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.BLOODHOUND, NpcId.BLOODHOUND, NpcId.BLOODHOUND_7232));
    return builder;
  }
}
