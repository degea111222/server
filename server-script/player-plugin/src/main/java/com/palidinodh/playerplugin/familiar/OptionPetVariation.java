package com.palidinodh.playerplugin.familiar;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OptionPetVariation implements PetVariation {

  private int chance;
}
