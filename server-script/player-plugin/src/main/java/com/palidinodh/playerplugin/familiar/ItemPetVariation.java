package com.palidinodh.playerplugin.familiar;

import com.palidinodh.osrscore.model.item.Item;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ItemPetVariation implements PetVariation {

  private Item item;
}
