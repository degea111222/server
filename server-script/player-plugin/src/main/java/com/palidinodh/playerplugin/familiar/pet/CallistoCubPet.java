package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class CallistoCubPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.CALLISTO_CUB, NpcId.CALLISTO_CUB, NpcId.CALLISTO_CUB_5558));
    return builder;
  }
}
