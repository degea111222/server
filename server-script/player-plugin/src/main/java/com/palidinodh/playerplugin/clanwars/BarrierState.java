package com.palidinodh.playerplugin.clanwars;

enum BarrierState {
  LOAD,
  DROP,
  DELETE
}
