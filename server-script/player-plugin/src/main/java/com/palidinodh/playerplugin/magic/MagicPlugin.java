package com.palidinodh.playerplugin.magic;

import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import lombok.Getter;
import lombok.Setter;

public class MagicPlugin implements PlayerPlugin {

  @Getter @Setter private transient int spellDelay;

  @Override
  public void tick() {
    if (spellDelay > 0) {
      spellDelay--;
    }
  }
}
