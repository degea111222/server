package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class LittleNightmarePet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.LITTLE_NIGHTMARE, NpcId.LITTLE_NIGHTMARE, NpcId.LITTLE_NIGHTMARE_9399));
    return builder;
  }
}
