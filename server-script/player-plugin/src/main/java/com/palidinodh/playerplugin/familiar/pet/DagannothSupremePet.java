package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class DagannothSupremePet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.PET_DAGANNOTH_SUPREME,
            NpcId.DAGANNOTH_SUPREME_JR,
            NpcId.DAGANNOTH_SUPREME_JR_6628));
    return builder;
  }
}
