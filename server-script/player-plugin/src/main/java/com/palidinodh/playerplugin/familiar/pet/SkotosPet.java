package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class SkotosPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.SKOTOS, NpcId.SKOTOS, NpcId.SKOTOS_7671));
    return builder;
  }
}
