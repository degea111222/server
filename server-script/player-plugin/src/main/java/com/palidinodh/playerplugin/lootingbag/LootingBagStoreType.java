package com.palidinodh.playerplugin.lootingbag;

public enum LootingBagStoreType {
  ASK,
  STORE_1,
  STORE_5,
  STORE_ALL,
  STORE_X
}
