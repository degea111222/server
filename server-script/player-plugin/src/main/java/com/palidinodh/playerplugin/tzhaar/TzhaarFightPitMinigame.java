package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.id.VarpId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Appearance;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.holidayboss.HolidayBossEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

public class TzhaarFightPitMinigame extends PEvent {

  private static final List<Tile> TZ_KIH_TILES =
      Arrays.asList(
          new Tile(2392, 5154),
          new Tile(2403, 5153),
          new Tile(2413, 5146),
          new Tile(2404, 5137),
          new Tile(2396, 5141),
          new Tile(2405, 5161),
          new Tile(2388, 5162),
          new Tile(2385, 5138),
          new Tile(2381, 5154),
          new Tile(2401, 5145));

  @Getter private static TzhaarFightPitMinigame instance;

  @Getter private static int winnerUserId;
  @Getter private static String winnerUsername = "N/A";

  @Inject private World world;
  private List<Player> lobbyPlayers = new ArrayList<>();
  private List<Player> fightPlayers = new ArrayList<>();
  private List<Npc> npcs = new ArrayList<>();
  private int time;
  @Getter private boolean givePrizes;

  public TzhaarFightPitMinigame(boolean givePrizes) {
    this.givePrizes = givePrizes;
  }

  @Override
  public void execute() {
    if (getExecutions() == 0) {
      setTick((int) PTime.minToTick(5));
      if (givePrizes) {
        world.sendBroadcast(
            "The TzHaar Fight Pit lobby is open, and it will begin in 5 minutes! Prizes include 5M.");
      } else {
        world.sendPvpNews("The TzHaar Fight Pit lobby is open! It will begin in 5 minutes!");
      }
      return;
    }
    if (getExecutions() == 1) {
      if (!moveLobby()) {
        stop();
      }
      return;
    }
    if (getExecutions() == 2) {
      startFight();
    }
    setTick(0);
    time++;
    checkNpcs();
    if (fightPlayers.size() <= 1) {
      stop();
    }
  }

  @Override
  public void stopHook() {
    if (fightPlayers.size() == 1) {
      var winner = fightPlayers.get(0);
      world.sendPvpNews(
          winner.getMessaging().getIconImage()
              + winner.getUsername()
              + " won the TzHaar Fight Pit!");
      winner.getAppearance().setSkullIcon(Appearance.PK_ICON_RED_SKULL);
      var previousWinner = world.getPlayerById(winnerUserId);
      if (previousWinner != null
          && previousWinner.getAppearance().getSkullIcon() == Appearance.PK_ICON_RED_SKULL) {
        previousWinner.getAppearance().setSkullIcon(-1);
      }
      winnerUserId = winner.getId();
      winnerUsername = winner.getUsername();
      removePlayer(winner);
    }
    var players = new ArrayList<Player>();
    players.addAll(lobbyPlayers);
    players.addAll(fightPlayers);
    lobbyPlayers.clear();
    fightPlayers.clear();
    players.forEach(
        p -> {
          if (!p.getController().isController(TzhaarFightPitController.class)) {
            return;
          }
          p.getController().stop();
        });
    world.removeNpcs(npcs);
    npcs.clear();
  }

  private boolean moveLobby() {
    if (lobbyPlayers.size() < 2) {
      return false;
    }
    setTick((int) PTime.secToTick(30));
    lobbyPlayers.forEach(
        p -> {
          p.getMovement().teleport(2399, 5166);
          p.getGameEncoder().sendMessage("<col=ff0000>Wait for my signal before fighting.</col>");
          p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, lobbyPlayers.size() - 1);
        });
    fightPlayers.addAll(lobbyPlayers);
    lobbyPlayers.clear();
    return true;
  }

  private void startFight() {
    fightPlayers.forEach(
        p -> {
          if (!p.getController().isController(TzhaarFightPitController.class)) {
            return;
          }
          var controller = p.getController().as(TzhaarFightPitController.class);
          controller.setState(TzhaarFightPitState.FIGHT);
          p.getGameEncoder().sendMessage("<col=ff0000>FIGHT!</col>");
        });
  }

  private void checkNpcs() {
    var seconds = PTime.tickToSec(time);
    if (seconds == 120 || seconds == 140 || seconds == 160) {
      for (var tile : TZ_KIH_TILES) {
        world.addNpc(new NpcSpawn(64, new Tile(tile).randomize(2), NpcId.TZ_KIH_22));
      }
    }
  }

  public void addPlayer(Player player) {
    lobbyPlayers.add(player);
    lobbyPlayers.forEach(
        p -> p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, lobbyPlayers.size() - 1));
  }

  public void removePlayer(Player player) {
    if (fightPlayers.contains(player) && givePrizes) {
      var items = getPrizes(world, fightPlayers.size() - 1);
      for (var item : items) {
        if (item.getId() == -1) {
          continue;
        }
        // TODO give the items
      }
    }
    lobbyPlayers.remove(player);
    fightPlayers.remove(player);
    lobbyPlayers.forEach(
        p -> p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, lobbyPlayers.size() - 1));
    fightPlayers.forEach(
        p -> p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, fightPlayers.size() - 1));
  }

  public static void start(boolean givePrizes) {
    if (isOpen() && !instance.isGivePrizes() && givePrizes) {
      instance.stop();
    }
    if (isOpen()) {
      return;
    }
    Main.getWorld().addEvent(instance = new TzhaarFightPitMinigame(givePrizes));
  }

  public static boolean isOpen() {
    return instance != null && instance.isRunning();
  }

  private static List<Item> getPrizes(World world, int position) {
    var holidayItemId = world.getWorldEvent(HolidayBossEvent.class).getItemId();
    List<Item> items = null;
    switch (position) {
      case 0:
        items = PCollection.toList(new Item(ItemId.COINS, 5_000_000), new Item(holidayItemId, 12));
        break;
      case 1:
        items = PCollection.toList(new Item(ItemId.COINS, 500_000), new Item(holidayItemId, 8));
        break;
      case 2:
        items = PCollection.toList(new Item(ItemId.COINS, 50_000), new Item(holidayItemId, 4));
        break;
    }
    return items;
  }
}
