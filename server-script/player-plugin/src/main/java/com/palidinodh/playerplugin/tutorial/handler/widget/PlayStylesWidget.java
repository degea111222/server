package com.palidinodh.playerplugin.tutorial.handler.widget;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.EnumMap;
import java.util.Map;

@ReferenceId(WidgetId.PLAY_STYLES_1011)
class PlayStylesWidget implements WidgetHandler {

  private static final Map<RsGameMode, String> GAME_MODE_DESCRIPTIONS =
      new EnumMap<>(RsGameMode.class);
  private static final Map<RsDifficultyMode, String> DIFFICULTY_MODE_DESCRIPTIONS =
      new EnumMap<>(RsDifficultyMode.class);

  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.getAttribute("game_mode_selected") == null) {
      player.putAttribute("game_mode_selected", RsGameMode.UNSET);
    }
    if (player.getAttribute("difficulty_mode_selected") == null) {
      player.putAttribute("difficulty_mode_selected", RsDifficultyMode.UNSET);
    }
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    switch (childId) {
      case 29:
        player.putAttribute("game_mode_selected", RsGameMode.REGULAR);
        break;
      case 32:
        player.putAttribute("game_mode_selected", RsGameMode.IRONMAN);
        player.putAttribute("difficulty_mode_selected", RsDifficultyMode.HARD);
        break;
      case 35:
        player.putAttribute("game_mode_selected", RsGameMode.HARDCORE_IRONMAN);
        player.putAttribute("difficulty_mode_selected", RsDifficultyMode.HARD);
        break;
      case 44:
        if (gameMode == RsGameMode.IRONMAN || gameMode == RsGameMode.HARDCORE_IRONMAN) {
          player.getGameEncoder().sendMessage("This mode can't choose a difficulty.");
          return;
        }
        player.putAttribute("difficulty_mode_selected", RsDifficultyMode.NORMAL);
        break;
      case 47:
        if (gameMode == RsGameMode.IRONMAN || gameMode == RsGameMode.HARDCORE_IRONMAN) {
          player.getGameEncoder().sendMessage("This mode can't choose a difficulty.");
          return;
        }
        player.putAttribute("difficulty_mode_selected", RsDifficultyMode.HARD);
        break;
      case 50:
        if (gameMode == RsGameMode.IRONMAN || gameMode == RsGameMode.HARDCORE_IRONMAN) {
          player.getGameEncoder().sendMessage("This mode can't choose a difficulty.");
          return;
        }
        player.putAttribute("difficulty_mode_selected", RsDifficultyMode.ELITE);
        break;
      case 18:
        accept(player);
        return;
    }
    gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    if (gameMode != RsGameMode.UNSET) {
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLES_1011, 22, gameMode.getFormattedName());
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLES_1011, 23, GAME_MODE_DESCRIPTIONS.get(gameMode));
    }
    var difficultyMode = (RsDifficultyMode) player.getAttribute("difficulty_mode_selected");
    if (difficultyMode != RsDifficultyMode.UNSET) {
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLES_1011, 24, difficultyMode.getFormattedName());
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.PLAY_STYLES_1011, 25, DIFFICULTY_MODE_DESCRIPTIONS.get(difficultyMode));
    }
  }

  private void accept(Player player) {
    if (!player.isGameModeUnset() || !player.isDifficultyModeUnset()) {
      player.getGameEncoder().sendMessage("Your account is already configured.");
      return;
    }
    player.getWidgetManager().removeInteractiveWidgets();
    player.unlock();
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    var difficultyMode = (RsDifficultyMode) player.getAttribute("difficulty_mode_selected");
    if (gameMode == null || difficultyMode == null) {
      return;
    }
    player.setDifficultyMode(difficultyMode);
    if (!player.isGameModeUnset()) {
      return;
    }
    player.setGameMode(gameMode);
    if (player.isGameModeUnset() || player.isDifficultyModeUnset()) {
      return;
    }
    player.getInventory().addItem(ItemId.STARTER_PACK_32288);
    player.getInventory().addItem(ItemId.COINS, 50_000);
    player.getInventory().addItem(ItemId.MONKFISH_NOTED, 100);
    player.getEquipment().setItem(Equipment.Slot.WEAPON, ItemId.IRON_SCIMITAR, 1);
    player.getEquipment().setItem(Equipment.Slot.NECK, ItemId.AMULET_OF_POWER, 1);
    player.getEquipment().setItem(Equipment.Slot.CAPE, ItemId.AVAS_ATTRACTOR, 1);
    if (player.isGameModeIronman() || player.isGameModeGroupIronman()) {
      player.getEquipment().setItem(Equipment.Slot.HEAD, ItemId.IRONMAN_HELM, 1);
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.IRONMAN_PLATEBODY, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.IRONMAN_PLATELEGS, 1);
    } else if (player.isGameModeHardcoreIronman()) {
      player.getEquipment().setItem(Equipment.Slot.HEAD, ItemId.HARDCORE_IRONMAN_HELM, 1);
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.HARDCORE_IRONMAN_PLATEBODY, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.HARDCORE_IRONMAN_PLATELEGS, 1);
    } else {
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.MONKS_ROBE_TOP, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.MONKS_ROBE, 1);
      player.getEquipment().setItem(Equipment.Slot.NECK, ItemId.AMULET_OF_GLORY_4, 1);
    }
    player.getEquipment().setItem(Equipment.Slot.HAND, ItemId.MITHRIL_GLOVES, 1);
    player.getEquipment().setItem(Equipment.Slot.FOOT, ItemId.CLIMBING_BOOTS, 1);
    player.getEquipment().setItem(Equipment.Slot.SHIELD, ItemId.UNHOLY_BOOK, 1);
    player.getEquipment().setUpdate(true);
    player.getAppearance().setUpdate(true);
    player
        .getGameEncoder()
        .sendMessage(
            "If you're not sure where to start, begin with the Training Day achievement diary!",
            Messaging.CHAT_TYPE_BROADCAST);
  }

  static {
    GAME_MODE_DESCRIPTIONS.put(RsGameMode.UNSET, "");
    GAME_MODE_DESCRIPTIONS.put(RsGameMode.REGULAR, "Play without any restrictions.");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.IRONMAN,
        "  * No trading, grand exchange, or staking.<br>  * Less items can be purchased from shops.");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.HARDCORE_IRONMAN,
        "  * No trading, grand exchange, or staking.<br>  * Less items can be purchased from shops.<br>  * On 'unsafe' death, your mode is reverted to ironman.");

    DIFFICULTY_MODE_DESCRIPTIONS.put(RsDifficultyMode.UNSET, "");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.NORMAL,
        "  * Combat experience multiplier: x100.<br>  * Skill experience multiplier: x20.");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.HARD,
        "  * Combat experience multiplier: x20.<br>  * Skill experience multiplier: x20.<br>  * Drop rate multiplier: TBD");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.ELITE,
        "  * Combat experience multiplier: x5.<br>  * Skill experience multiplier: x5.<br>  * Drop rate multiplier: TBD.");
  }
}
