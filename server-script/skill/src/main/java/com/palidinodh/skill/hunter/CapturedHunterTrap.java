package com.palidinodh.skill.hunter;

import com.palidinodh.osrscore.model.item.RandomItem;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class CapturedHunterTrap {

  private int id;
  private int level;
  private int experience;
  private RandomItem[] items;
}
