package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("objanim")
class ObjectAnimationCommand implements CommandHandler, CommandHandler.BetaWorld {

  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var mapObject = player.getController().getSolidMapObject(player);
    if (mapObject == null) {
      return;
    }
    player.getGameEncoder().sendMapObjectAnimation(mapObject, Integer.parseInt(message));
  }
}
