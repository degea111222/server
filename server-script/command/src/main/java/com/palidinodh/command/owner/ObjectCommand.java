package com.palidinodh.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("obj")
class ObjectCommand implements CommandHandler, CommandHandler.OwnerRank {

  @Override
  public String getExample(String name) {
    return "id type direction";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    var id = messages[0];
    var type = messages[1];
    var direction = messages[2];
    var mapObject = new MapObject(id, type, direction, player);
    player.getController().addMapObject(mapObject);
  }
}
