package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"script", "scriptloop"})
class ScriptCommand implements CommandHandler, CommandHandler.BetaWorld {

  @Override
  public String getExample(String name) {
    switch (name) {
      case "script":
        return "id";
      case "scriptloop":
        return "start_id end_id";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    switch (name) {
      case "script":
        player.getGameEncoder().sendScript(messages[0]);
        break;
      case "scriptloop":
        for (var i = messages[0]; i < messages[1]; i++) {
          player.getGameEncoder().sendScript(i);
        }
        break;
    }
  }
}
