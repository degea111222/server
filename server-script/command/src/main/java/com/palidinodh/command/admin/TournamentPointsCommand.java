package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("tournamentpoints")
class TournamentPointsCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "\"username\" amount";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var amount = Integer.parseInt(messages[1]);
    var player2 = player.getWorld().getPlayerByUsername(username);
    var plugin = player2.getPlugin(ClanWarsPlugin.class);
    if (amount == 0) {
      plugin.setPoints(0);
      player.getGameEncoder().sendMessage("Reset tournament points for " + player2.getUsername());
      player2.getGameEncoder().sendMessage(player.getUsername() + " reset your tournament points.");
    } else {
      plugin.setPoints(plugin.getPoints() + amount);
      player
          .getGameEncoder()
          .sendMessage("Added " + amount + " tournament points to " + player2.getUsername());
      player2
          .getGameEncoder()
          .sendMessage(
              player.getUsername() + " added " + amount + " tournament points to your account.");
    }
  }
}
