package com.palidinodh.command.seniormod;

import com.palidinodh.io.JavaCord;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PLogger;

@ReferenceName("shutdown")
class ShutdownCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "minutes";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var minutes = Integer.parseInt(message);
    player.getWorld().startShutdown(minutes);
    PLogger.println(
        player.getUsername()
            + " shut the server down with a countdown of "
            + minutes
            + " minutes.");
    JavaCord.sendMessage(
        DiscordChannel.MODERATION,
        player.getUsername()
            + " shut the server down with a countdown of "
            + minutes
            + " minutes.");
  }
}
