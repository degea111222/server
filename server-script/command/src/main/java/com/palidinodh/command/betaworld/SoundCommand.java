package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("sound")
class SoundCommand implements CommandHandler, CommandHandler.BetaWorld {

  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.getController().sendMapSound(Integer.parseInt(message));
  }
}
