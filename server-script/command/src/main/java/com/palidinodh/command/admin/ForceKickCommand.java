package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("fkick")
class ForceKickCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var targetPlayer = player.getWorld().getPlayerByUsername(message);
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    targetPlayer.unlock();
    targetPlayer.getGameEncoder().sendMessage(player.getUsername() + " has kicked you.");
    targetPlayer.getGameEncoder().sendLogout();
    targetPlayer.setVisible(false);
    player.getGameEncoder().sendMessage(message + " has been kicked.");
    player.log(PlayerLogType.STAFF, "forced-kicked " + targetPlayer.getLogName());
  }
}
