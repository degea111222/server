package com.palidinodh.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("skin")
class SkinCommand implements CommandHandler, CommandHandler.OwnerRank {

  @Override
  public String getExample(String name) {
    return "id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var id = Integer.parseInt(message);
    player.getAppearance().setColor(4, id);
  }
}
