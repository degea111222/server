package com.palidinodh.command.all;

import com.palidinodh.osrscore.io.cache.id.ScriptId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Options;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"resizeline", "resizebox", "resizemobile"})
class ResizeableCommand implements CommandHandler {

  @Override
  public void execute(Player player, String name, String message) {
    player.getOptions().setClickThroughChatbox(true);
    player.getOptions().setTransparentChatbox(true);
    player.getOptions().setOpaqueSidePanel(false);
    switch (name) {
      case "resizeline":
        {
          player.getOptions().setResizeType(Options.ResizeType.LINE);
          break;
        }
      case "resizebox":
        {
          player.getOptions().setResizeType(Options.ResizeType.BOX);
          break;
        }
      case "resizemobile":
        {
          player.getOptions().setResizeType(Options.ResizeType.MOBILE);
          break;
        }
    }
    player.getGameEncoder().sendScript(ScriptId.SET_VIEWPORT_RESIZABLE);
  }
}
