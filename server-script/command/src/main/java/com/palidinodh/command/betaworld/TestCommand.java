package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("test")
class TestCommand implements CommandHandler, CommandHandler.BetaWorld {

  // Bullshit class to test things out combined with hot code replace
  @Override
  public void execute(Player player, String name, String message) {

    player.getWidgetManager().sendInteractiveOverlay(1014);

    player
        .getGameEncoder()
        .sendWidgetSettings(
            1014,
            32,
            0,
            1000,
            WidgetSetting.OPTION_0,
            WidgetSetting.OPTION_1,
            WidgetSetting.OPTION_2,
            WidgetSetting.OPTION_3,
            WidgetSetting.OPTION_4,
            WidgetSetting.OPTION_5,
            WidgetSetting.OPTION_9);

    player.getGameEncoder().sendScript(8194, 1, 100_000, -1, 4151, 0);
    player.getGameEncoder().sendScript(8194, 2, 1_000_000, 0, 4151, 1);
    player.getGameEncoder().sendScript(8194, 0, 100_000_000, 1, 4151, 2);

    player.getGameEncoder().sendScript(8194, 1, 100_000, -1, 554, 3);
    player.getGameEncoder().sendScript(8194, 2, 1_000_000, 0, 554, 4);
    player.getGameEncoder().sendScript(8194, 0, 100_000_000, 1, 554, 5);

    player.getGameEncoder().sendScript(8194, 1, 100_000, -1, 4153, 6);
    player.getGameEncoder().sendScript(8194, 2, 1_000_000, 0, 4153, 7);
    player.getGameEncoder().sendScript(8194, 0, 100_000_000, 1, 4153, 8);

    // player.getInventory().sendItems(-1, 525);
    // player.getGameEncoder().sendScript(3478, 10, 0); // death then fee
    // player.getWidgetManager().sendInteractiveOverlay(672);

    // player.getWidgetManager().sendWorldMap(true);
    // player.getGameEncoder().sendScript(2264);

    // player.getGameEncoder().setVarbit(6352, 1);

    // player.getGameEncoder().sendScript(1490, 601 << 16 | 6);

    // player.getGameEncoder().sendWidgetText(WidgetId.FIGHT_PIT_OVERLAY, 5, "test");

    // player.getGameEncoder().sendScript(847, 261 << 16 | 109, 261 << 16 | 34, 261 << 16 | 33, 2);

    // player.getGameEncoder().setVarbit(6352, 1);
  }
}
