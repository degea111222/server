package com.palidinodh.npccombatv0;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class IceWarrior57_2851Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ICE_WARRIOR_57_2851);
    combat.hitpoints(NpcCombatHitpoints.total(59));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(47)
            .defenceLevel(47)
            .bonus(BonusType.DEFENCE_STAB, 30)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 10)
            .bonus(BonusType.DEFENCE_RANGED, 30)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(843).blockAnimation(404);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(6));
    style.animation(451).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
