package com.palidinodh.npccombatv0;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import java.util.Arrays;
import java.util.List;

class ChaosElemental305Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CHAOS_ELEMENTAL_305);

    return Arrays.asList(combat.build());
  }
}
