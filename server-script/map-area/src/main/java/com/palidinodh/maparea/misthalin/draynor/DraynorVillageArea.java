package com.palidinodh.maparea.misthalin.draynor;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12338, 12339, 12595})
public class DraynorVillageArea extends Area {}
