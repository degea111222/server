package com.palidinodh.maparea.misthalin.draynor.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DraynorVillageNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(3079, 3256), NpcId.MARTIN_THE_MASTER_GARDENER));
    spawns.add(new NpcSpawn(2, new Tile(3075, 3259), NpcId.PIG_2812));
    spawns.add(new NpcSpawn(2, new Tile(3079, 3259), NpcId.PIG));
    spawns.add(new NpcSpawn(2, new Tile(3078, 3261), NpcId.PIGLET));
    spawns.add(new NpcSpawn(2, new Tile(3076, 3261), NpcId.PIGLET_2814));
    spawns.add(new NpcSpawn(2, new Tile(3077, 3260), NpcId.PIGLET_2815));
    spawns.add(new NpcSpawn(2, new Tile(3077, 3263), NpcId.PIG));
    spawns.add(new NpcSpawn(2, new Tile(3072, 3260), NpcId.PIG));
    spawns.add(new NpcSpawn(2, new Tile(3077, 3256), NpcId.PIG));
    spawns.add(new NpcSpawn(4, new Tile(3078, 3250), NpcId.TOWN_CRIER_277));
    spawns.add(new NpcSpawn(4, new Tile(3077, 3252), NpcId.OLIVIA));
    spawns.add(new NpcSpawn(4, new Tile(3077, 3246), NpcId.DIANGO));
    spawns.add(new NpcSpawn(4, new Tile(3085, 3250), NpcId.FORTUNATO));
    spawns.add(new NpcSpawn(4, new Tile(3086, 3245), NpcId.MASTER_FARMER));
    spawns.add(new NpcSpawn(4, new Tile(3082, 3249), NpcId.MARKET_GUARD_20));
    spawns.add(new NpcSpawn(2, new Tile(3088, 3248), NpcId.BANK_GUARD));
    spawns.add(new NpcSpawn(2, new Tile(3086, 3258), NpcId.AGGIE));
    spawns.add(new NpcSpawn(4, new Tile(3079, 3251), NpcId.MARKET_GUARD_20));
    spawns.add(new NpcSpawn(new Tile(3088, 3255), NpcId.WISE_OLD_MAN));
    spawns.add(new NpcSpawn(new Tile(3095, 3252), NpcId.MISS_SCHISM));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3090, 3245), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3090, 3243), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3090, 3242), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(new Tile(3088, 3242), NpcId.BANKER));
    spawns.add(new NpcSpawn(2, new Tile(3099, 3257), NpcId.NED));
    spawns.add(new NpcSpawn(2, new Tile(3096, 3227), NpcId.TWIGGY_OKORN));
    spawns.add(new NpcSpawn(4, new Tile(3097, 3219), NpcId.BLACK_KNIGHT_33));
    spawns.add(new NpcSpawn(8, new Tile(3099, 3226), NpcId.SQUIRREL_1418));
    spawns.add(new NpcSpawn(4, new Tile(3090, 3232), NpcId.DARK_WIZARD_7));
    spawns.add(new NpcSpawn(4, new Tile(3084, 3235), NpcId.DARK_WIZARD_7));

    return spawns;
  }
}
