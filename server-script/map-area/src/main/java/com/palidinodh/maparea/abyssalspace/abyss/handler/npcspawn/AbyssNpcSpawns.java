package com.palidinodh.maparea.abyssalspace.abyss.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AbyssNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3033, 4853), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3044, 4853), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3054, 4850), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3061, 4841), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3062, 4826), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3059, 4814), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3044, 4810), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3027, 4811), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3017, 4821), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3016, 4835), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3020, 4847), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3025, 4851), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3039, 4855), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3049, 4852), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3059, 4846), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3062, 4832), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3060, 4819), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3051, 4812), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3036, 4810), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3021, 4814), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3015, 4829), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3017, 4840), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3016, 4824), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(5, new Tile(3017, 4810), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3039, 4807), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3057, 4811), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3059, 4824), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3064, 4840), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(5, new Tile(3053, 4855), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3042, 4855), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3035, 4855), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3059, 4850), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3060, 4836), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3064, 4822), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3054, 4814), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3031, 4808), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3017, 4816), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3013, 4838), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3015, 4848), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3018, 4843), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3029, 4854), NpcId.ABYSSAL_LEECH_41));
    spawns.add(new NpcSpawn(4, new Tile(3047, 4809), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3059, 4817), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3062, 4828), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3060, 4840), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3053, 4850), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3027, 4851), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3020, 4843), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3019, 4819), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3026, 4807), NpcId.ABYSSAL_GUARDIAN_59));
    spawns.add(new NpcSpawn(4, new Tile(3033, 4810), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3046, 4811), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3054, 4810), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3061, 4810), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3061, 4823), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3062, 4836), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3056, 4848), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3049, 4855), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3030, 4856), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3022, 4850), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3016, 4838), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3016, 4832), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3017, 4813), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3024, 4812), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3030, 4810), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3041, 4810), NpcId.ABYSSAL_WALKER_81));
    spawns.add(new NpcSpawn(4, new Tile(3039, 4835), NpcId.MAGE_OF_ZAMORAK_7425));

    return spawns;
  }
}
