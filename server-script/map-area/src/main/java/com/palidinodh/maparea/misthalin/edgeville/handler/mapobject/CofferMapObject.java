package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.osrscore.io.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;

@ReferenceId(ObjectId.COFFER)
class CofferMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getWorld().getWorldEvent(PvpTournamentEvent.class).openCofferDialogue(player);
  }

  @Override
  public void itemOnMapObject(Player player, int slot, int itemId, MapObject mapObject) {
    player
        .getWorld()
        .getWorldEvent(PvpTournamentEvent.class)
        .itemOnCoffer(player, player.getInventory().getItem(slot));
  }
}
