package com.palidinodh.maparea.dragonkinislands.wyverncave.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class WyvernCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3605, 10215), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3613, 10214), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3612, 10205), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3604, 10202), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3598, 10186), NpcId.LONG_TAILED_WYVERN_152));
    spawns.add(new NpcSpawn(4, new Tile(3604, 10191), NpcId.LONG_TAILED_WYVERN_152));
    spawns.add(new NpcSpawn(4, new Tile(3597, 10196), NpcId.LONG_TAILED_WYVERN_152));
    spawns.add(new NpcSpawn(4, new Tile(3630, 10196), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3627, 10187), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3620, 10183), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3621, 10193), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3633, 10212), NpcId.ANCIENT_WYVERN_210));
    spawns.add(new NpcSpawn(4, new Tile(3635, 10206), NpcId.ANCIENT_WYVERN_210));
    spawns.add(new NpcSpawn(4, new Tile(3626, 10246), NpcId.ANCIENT_WYVERN_210));
    spawns.add(new NpcSpawn(4, new Tile(3636, 10246), NpcId.ANCIENT_WYVERN_210));
    spawns.add(new NpcSpawn(4, new Tile(3632, 10253), NpcId.ANCIENT_WYVERN_210));
    spawns.add(new NpcSpawn(4, new Tile(3604, 10265), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3602, 10273), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3613, 10273), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3611, 10281), NpcId.SPITTING_WYVERN_139));
    spawns.add(new NpcSpawn(4, new Tile(3622, 10291), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3626, 10285), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3634, 10276), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3636, 10283), NpcId.TALONED_WYVERN_147));
    spawns.add(new NpcSpawn(4, new Tile(3596, 10249), NpcId.LONG_TAILED_WYVERN_152));
    spawns.add(new NpcSpawn(4, new Tile(3603, 10251), NpcId.LONG_TAILED_WYVERN_152));
    spawns.add(new NpcSpawn(4, new Tile(3614, 10249), NpcId.LONG_TAILED_WYVERN_152));
    spawns.add(new NpcSpawn(4, new Tile(3609, 10252), NpcId.LONG_TAILED_WYVERN_152));

    return spawns;
  }
}
