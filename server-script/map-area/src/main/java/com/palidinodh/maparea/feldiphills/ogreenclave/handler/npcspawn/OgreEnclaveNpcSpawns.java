package com.palidinodh.maparea.feldiphills.ogreenclave.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class OgreEnclaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2592, 9461), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2607, 9459), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2607, 9434), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2594, 9431), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2587, 9424), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2581, 9435), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2569, 9435), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2572, 9447), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2582, 9459), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2613, 9444), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2614, 9421), NpcId.BLUE_DRAGON_111));

    return spawns;
  }
}
