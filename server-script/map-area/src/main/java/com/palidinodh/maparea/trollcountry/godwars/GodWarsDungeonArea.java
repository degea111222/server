package com.palidinodh.maparea.trollcountry.godwars;

import com.palidinodh.osrscore.io.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.id.VarbitId;
import com.palidinodh.osrscore.io.cache.id.WidgetId;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.UserRank;
import lombok.Getter;
import lombok.Setter;

@ReferenceId({11346, 11347, 11602, 11603})
@Getter
@Setter
public class GodWarsDungeonArea extends Area {

  private int armadylKillcount;
  private int bandosKillcount;
  private int zamorakKillcount;
  private int saradominKillcount;
  private int altarDelay;

  @Override
  public Object script(String name, Object... args) {
    var player = getPlayer();
    switch (name) {
      case "increase_armadyl_killcount":
        {
          armadylKillcount++;
          if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
            armadylKillcount++;
          }
          break;
        }
      case "increase_bandos_killcount":
        {
          bandosKillcount++;
          if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
            bandosKillcount++;
          }
          break;
        }
      case "increase_saradomin_killcount":
        {
          saradominKillcount++;
          if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
            saradominKillcount++;
          }
          break;
        }
      case "increase_zamorak_killcount":
        {
          zamorakKillcount++;
          if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
            zamorakKillcount++;
          }
          break;
        }
      case "has_armadyl_killcount":
        return armadylKillcount >= 40 || player.getInventory().hasItem(ItemId.ECUMENICAL_KEY);
      case "has_bandos_killcount":
        return bandosKillcount >= 40 || player.getInventory().hasItem(ItemId.ECUMENICAL_KEY);
      case "has_saradomin_killcount":
        return saradominKillcount >= 40 || player.getInventory().hasItem(ItemId.ECUMENICAL_KEY);
      case "has_zamorak_killcount":
        return zamorakKillcount >= 40 || player.getInventory().hasItem(ItemId.ECUMENICAL_KEY);
      case "clear_armadyl_killcount":
        {
          if (armadylKillcount >= 40) {
            armadylKillcount -= 40;
          } else {
            player.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
          }
          break;
        }
      case "clear_bandos_killcount":
        {
          if (bandosKillcount >= 40) {
            bandosKillcount -= 40;
          } else {
            player.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
          }
          break;
        }
      case "clear_saradomin_killcount":
        {
          if (saradominKillcount >= 40) {
            saradominKillcount -= 40;
          } else {
            player.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
          }
          break;
        }
      case "clear_zamorak_killcount":
        {
          if (zamorakKillcount >= 40) {
            zamorakKillcount -= 40;
          } else {
            player.getInventory().deleteItem(ItemId.ECUMENICAL_KEY);
          }
          break;
        }
    }
    sendOverlay();
    return null;
  }

  @Override
  public void loadPlayer() {
    var player = getPlayer();
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_ENTRANCE_ROPE, 1);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMIN_FIRST_ROPE, 1);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMIN_SECOND_ROPE, 1);
  }

  @Override
  public void unloadPlayer() {
    var player = getPlayer();
    player.getWidgetManager().removeFullOverlay();
  }

  @Override
  public void tickPlayer() {
    var player = getPlayer();
    if (player.getWidgetManager().getFullOverlay() != WidgetId.GOD_WARS_OVERLAY) {
      player.getWidgetManager().sendFullOverlay(WidgetId.GOD_WARS_OVERLAY);
      sendOverlay();
    }
    altarDelay -= altarDelay > 0 ? 1 : 0;
  }

  private void sendOverlay() {
    var player = getPlayer();
    if (player.getCombat().getSaradominsLight()) {
      player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMINS_LIGHT, 1);
    }
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_SARADOMIN_KILLS, saradominKillcount);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_ARMADYL_KILLS, armadylKillcount);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_BANDOS_KILLS, bandosKillcount);
    player.getGameEncoder().setVarbit(VarbitId.GOD_WARS_ZAMORAK_KILLS, zamorakKillcount);
  }
}
