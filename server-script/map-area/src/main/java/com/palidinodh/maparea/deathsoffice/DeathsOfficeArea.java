package com.palidinodh.maparea.deathsoffice;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12633)
public class DeathsOfficeArea extends Area {}
