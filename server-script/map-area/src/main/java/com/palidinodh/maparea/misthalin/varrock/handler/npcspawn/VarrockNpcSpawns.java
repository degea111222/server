package com.palidinodh.maparea.misthalin.varrock.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class VarrockNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3229, 3454), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(3226, 3457), NpcId.TREZNOR));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3222, 3394), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3224, 3394), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(new Tile(3226, 3397), NpcId.TANNER));

    return spawns;
  }
}
