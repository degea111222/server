package com.palidinodh.maparea.misthalin.edgeville.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.setting.Settings;
import java.util.ArrayList;
import java.util.List;

class EdgevilleNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(3101, 3508), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3508), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3510), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3512), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3514), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3516), NpcId.BANKER_395));
    spawns.add(
        new NpcSpawn(Tile.Direction.NORTH, new Tile(3087, 3510), NpcId.GRAND_EXCHANGE_CLERK_2149));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3088, 3508), NpcId.GRAND_EXCHANGE_CLERK));
    spawns.add(
        new NpcSpawn(Tile.Direction.EAST, new Tile(3088, 3506), NpcId.GRAND_EXCHANGE_CLERK_2149));
    spawns.add(new NpcSpawn(new Tile(3099, 3507), NpcId.VOTE_MANAGER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3513), NpcId.BOB_BARTER_HERBS));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3091, 3505), NpcId.CAPT_BOND_16018));
    spawns.add(new NpcSpawn(new Tile(3089, 3516), NpcId.LOYALTY_MANAGER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3107, 3510), NpcId.EMBLEM_TRADER_316));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3107, 3512), NpcId.MAGE_OF_ZAMORAK_2582));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3107, 3514), NpcId.PERDU));
    spawns.add(new NpcSpawn(new Tile(3114, 3516), NpcId.COMBAT_DUMMY));
    spawns.add(new NpcSpawn(new Tile(3115, 3516), NpcId.COMBAT_DUMMY));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3077, 3500), NpcId.HEAD_CHEF));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3077, 3493), NpcId.AJJAT));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3077, 3486), NpcId.PROBITA));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3077, 3479), NpcId.HAIRDRESSER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3084, 3479), NpcId.TWIGGY_OKORN));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3084, 3486), NpcId.ADAM));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3095, 3483), NpcId.KRYSTILIA));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3092, 3483), NpcId.NIEVE));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3116, 3487), NpcId.SKILLING_SELLER));
    spawns.add(new NpcSpawn(new Tile(3114, 3490), NpcId.TANNER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3113, 3485), NpcId.MAC_126));
    spawns.add(new NpcSpawn(new Tile(3108, 3494), NpcId.MARTIN_THWAIT));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3092, 3472), NpcId.WISE_OLD_MAN));
    spawns.add(new NpcSpawn(2, new Tile(3069, 3517), NpcId.OZIACH));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3506, 1), NpcId.JOSSIK));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3507, 1), NpcId.EVIL_DAVE_4806));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3508, 1), NpcId.RADIMUS_ERKLE));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3509, 1), NpcId.GUILDMASTER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3510, 1), NpcId.MONK_OF_ENTRANA));
    spawns.add(
        new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3511, 1), NpcId.KING_NARNODE_SHAREEN));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3512, 1), NpcId.ONEIROMANCER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3513, 1), NpcId.ZEALOT));
    spawns.add(
        new NpcSpawn(Tile.Direction.EAST, new Tile(3087, 3514, 1), NpcId.CAPN_IZZY_NO_BEARD));
    spawns.add(new NpcSpawn(4, new Tile(3100, 3514, 1), NpcId.PARTY_PETE));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3094, 3506, 1), NpcId.MEGAN));
    spawns.add(new NpcSpawn(2, new Tile(3098, 3506, 1), NpcId.LUCY));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3091, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3092, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3093, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3094, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(new Tile(3098, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(new Tile(3097, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(new Tile(3096, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(new Tile(3095, 3512, 1), NpcId.KNIGHT_5793));
    if (Settings.getInstance().isLocal() || Settings.getInstance().isBeta()) {
      spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3093, 3504), NpcId.ELISABETA));
    }

    return spawns;
  }
}
