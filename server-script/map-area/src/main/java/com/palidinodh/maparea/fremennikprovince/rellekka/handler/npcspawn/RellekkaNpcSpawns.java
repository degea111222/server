package com.palidinodh.maparea.fremennikprovince.rellekka.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class RellekkaNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2644, 3662), NpcId.HUNTING_EXPERT_1504));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2648, 3657), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2649, 3661), NpcId.BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2669, 3725), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2672, 3728), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2676, 3730), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2681, 3731), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2685, 3729), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2681, 3725), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2686, 3723), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2686, 3719), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2686, 3714), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2679, 3714), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2679, 3720), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2673, 3718), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2674, 3712), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2667, 3711), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(4, new Tile(2665, 3716), NpcId.ROCK_CRAB_13));
    spawns.add(new NpcSpawn(new Tile(2660, 3646), NpcId.TOWN_GUARD));
    spawns.add(new NpcSpawn(3, new Tile(2664, 3646), NpcId.TOWN_GUARD));
    spawns.add(new NpcSpawn(4, new Tile(2661, 3653), NpcId.SIGLI_THE_HUNTSMAN));
    spawns.add(new NpcSpawn(4, new Tile(2668, 3652), NpcId.PONTAK_48));
    spawns.add(new NpcSpawn(2, new Tile(2655, 3651), NpcId.LENSA_48));
    spawns.add(new NpcSpawn(new Tile(2657, 3663), NpcId.GUARD_3928));
    spawns.add(new NpcSpawn(new Tile(2660, 3663), NpcId.GUARD_3928));
    spawns.add(new NpcSpawn(4, new Tile(2658, 3660), NpcId.ASKELADDEN));
    spawns.add(new NpcSpawn(4, new Tile(2659, 3669), NpcId.BRUNDT_THE_CHIEFTAIN));
    spawns.add(new NpcSpawn(4, new Tile(2657, 3673), NpcId.BJORN));
    spawns.add(new NpcSpawn(4, new Tile(2660, 3673), NpcId.MANNI_THE_REVELLER));
    spawns.add(new NpcSpawn(2, new Tile(2662, 3672), NpcId.THORA_THE_BARKEEP));
    spawns.add(new NpcSpawn(4, new Tile(2659, 3677), NpcId.ELDGRIM));
    spawns.add(new NpcSpawn(new Tile(2657, 3680), NpcId.STYRMIR));
    spawns.add(new NpcSpawn(new Tile(2660, 3680), NpcId.OSPAK));
    spawns.add(new NpcSpawn(new Tile(2658, 3679), NpcId.TORBRUND));
    spawns.add(new NpcSpawn(new Tile(2659, 3678), NpcId.FRIDGEIR));
    spawns.add(new NpcSpawn(4, new Tile(2647, 3659), NpcId.SWENSEN_THE_NAVIGATOR));
    spawns.add(new NpcSpawn(4, new Tile(2642, 3651), NpcId.JENNELLA_48));
    spawns.add(new NpcSpawn(4, new Tile(2629, 3653), NpcId.SASSILIK_48));
    spawns.add(new NpcSpawn(4, new Tile(2634, 3669), NpcId.PEER_THE_SEER_8147));
    spawns.add(new NpcSpawn(4, new Tile(2624, 3673), NpcId.YRSA));
    spawns.add(new NpcSpawn(8, new Tile(2643, 3675), NpcId.AGNAR));
    spawns.add(new NpcSpawn(4, new Tile(2645, 3674), NpcId.FISH_MONGER));
    spawns.add(new NpcSpawn(4, new Tile(2641, 3678), NpcId.FUR_TRADER_3948));
    spawns.add(new NpcSpawn(4, new Tile(2644, 3680), NpcId.SIGMUND_THE_MERCHANT));
    spawns.add(new NpcSpawn(4, new Tile(2640, 3697), NpcId.TORFINN));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2641, 3699), NpcId.FISHERMAN));
    spawns.add(new NpcSpawn(4, new Tile(2630, 3692), NpcId.SAILOR_3936));
    spawns.add(new NpcSpawn(new Tile(2621, 3688), NpcId.LOKAR_SEARUNNER));
    spawns.add(new NpcSpawn(new Tile(2620, 3684), NpcId.JARVALD_7205));
    spawns.add(new NpcSpawn(4, new Tile(2658, 3692), NpcId.VOLF_OLAFSON));
    spawns.add(new NpcSpawn(4, new Tile(2664, 3694), NpcId.SKULGRIMEN));
    spawns.add(new NpcSpawn(4, new Tile(2665, 3691), NpcId.THORVALD_THE_WARRIOR));
    spawns.add(new NpcSpawn(4, new Tile(2660, 3701), NpcId.DRON));
    spawns.add(new NpcSpawn(4, new Tile(2669, 3702), NpcId.FREYGERD_48));
    spawns.add(new NpcSpawn(4, new Tile(2663, 3708), NpcId.REESO));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2644, 3709), NpcId.MORD_GUNNARS));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2644, 3710), NpcId.MARIA_GUNNARS_1883));
    spawns.add(new NpcSpawn(4, new Tile(2680, 3690), NpcId.BORROKAR_48));
    spawns.add(new NpcSpawn(4, new Tile(2669, 3684), NpcId.LONGHALL_BOUNCER));
    spawns.add(new NpcSpawn(4, new Tile(2673, 3683), NpcId.OLAF_THE_BARD));
    spawns.add(new NpcSpawn(4, new Tile(2675, 3675), NpcId.FREIDIR_48));
    spawns.add(new NpcSpawn(4, new Tile(2676, 3677), NpcId.INGA_48));
    spawns.add(new NpcSpawn(4, new Tile(2673, 3670), NpcId.BLANIN));
    spawns.add(new NpcSpawn(4, new Tile(2675, 3665), NpcId.LANZIG_48));
    spawns.add(new NpcSpawn(4, new Tile(2677, 3666), NpcId.INGRID_HRADSON));

    return spawns;
  }
}
