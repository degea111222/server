package com.palidinodh.maparea.abyssalspace.abyssalarea.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AbyssalAreaNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3052, 4874), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3045, 4874), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3038, 4871), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3030, 4874), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3019, 4878), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3016, 4891), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3023, 4897), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3036, 4883), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3029, 4889), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3044, 4885), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3050, 4892), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3062, 4886), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3065, 4898), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3060, 4906), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3056, 4911), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3051, 4918), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3043, 4914), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3049, 4900), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3041, 4905), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3033, 4911), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3023, 4916), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3019, 4907), NpcId.ABYSSAL_DEMON_124));
    spawns.add(new NpcSpawn(4, new Tile(3032, 4902), NpcId.ABYSSAL_DEMON_124));

    return spawns;
  }
}
