package com.palidinodh.maparea.wilderness.scorpiascave.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class ScorpiasCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3223, 10347), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3225, 10348), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3223, 10344), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3224, 10339), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3221, 10335), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3225, 10333), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3229, 10333), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3229, 10337), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3228, 10342), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3229, 10347), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3232, 10349), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3236, 10349), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3233, 10344), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3234, 10338), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3232, 10334), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3236, 10335), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3240, 10334), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3244, 10335), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3240, 10337), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3243, 10344), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3243, 10348), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3238, 10346), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3238, 10341), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(4, new Tile(3243, 10339), NpcId.SCORPIAS_OFFSPRING_15));
    spawns.add(new NpcSpawn(8, new Tile(3231, 10340), NpcId.SCORPIA_225));

    return spawns;
  }
}
