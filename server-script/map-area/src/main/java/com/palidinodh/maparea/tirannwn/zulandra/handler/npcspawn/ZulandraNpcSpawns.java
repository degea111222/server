package com.palidinodh.maparea.tirannwn.zulandra.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class ZulandraNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2196, 3057), NpcId.ZUL_ANIEL));
    spawns.add(new NpcSpawn(4, new Tile(2197, 3061), NpcId.ZUL_ARETH));
    spawns.add(new NpcSpawn(new Tile(2200, 3054), NpcId.ZUL_URGISH));
    spawns.add(new NpcSpawn(new Tile(2192, 3055), NpcId.HIGH_PRIESTESS_ZUL_HARCINQA));
    spawns.add(new NpcSpawn(4, new Tile(2211, 3056), NpcId.PRIESTESS_ZUL_GWENWYNIG_2033));

    return spawns;
  }
}
