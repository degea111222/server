package com.palidinodh.maparea.essencemine.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class EssenceMineNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2907, 4835), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2914, 4831), NpcId.BANKER_395));

    return spawns;
  }
}
