package com.palidinodh.maparea.voidknightsoutpost.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class VoidKnightsOutpostNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2661, 2648), NpcId.VOID_KNIGHT));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2657, 2637), NpcId.SQUIRE_NOVICE));
    spawns.add(new NpcSpawn(1, new Tile(2668, 2651), NpcId.SQUIRE_1764));
    spawns.add(new NpcSpawn(1, new Tile(2664, 2660), NpcId.SQUIRE_1765));
    spawns.add(new NpcSpawn(1, new Tile(2658, 2654), NpcId.SQUIRE_1767));
    spawns.add(new NpcSpawn(1, new Tile(2651, 2659), NpcId.SQUIRE_1766));
    spawns.add(new NpcSpawn(1, new Tile(2651, 2664), NpcId.SQUIRE_1768));
    spawns.add(new NpcSpawn(4, new Tile(2659, 2673), NpcId.SQUIRE_1770));
    spawns.add(new NpcSpawn(4, new Tile(2657, 2663), NpcId.SQUIRE));
    spawns.add(new NpcSpawn(4, new Tile(2660, 2658), NpcId.SQUIRE_1761));

    return spawns;
  }
}
