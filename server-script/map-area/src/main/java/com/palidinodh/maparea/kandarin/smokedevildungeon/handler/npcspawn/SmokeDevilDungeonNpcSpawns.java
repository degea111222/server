package com.palidinodh.maparea.kandarin.smokedevildungeon.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class SmokeDevilDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2382, 9430), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2387, 9433), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2383, 9437), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2384, 9443), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2388, 9446), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2383, 9449), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2383, 9454), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2381, 9461), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2382, 9466), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2387, 9451), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2388, 9457), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2391, 9462), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2396, 9462), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2399, 9458), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2400, 9453), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2394, 9451), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2393, 9446), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2393, 9440), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2393, 9432), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2390, 9427), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2391, 9422), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2397, 9429), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2398, 9435), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2399, 9440), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2399, 9447), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2404, 9448), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2406, 9453), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2405, 9459), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2405, 9439), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2404, 9433), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2402, 9427), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2400, 9422), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2410, 9422), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2408, 9427), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2409, 9433), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2418, 9420), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2423, 9423), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2420, 9426), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2420, 9431), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2422, 9436), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2424, 9443), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2412, 9440), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2411, 9446), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2418, 9447), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2422, 9451), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2423, 9457), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2420, 9460), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2415, 9462), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2416, 9456), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2410, 9460), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(4, new Tile(2412, 9451), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(8, new Tile(2363, 9449), NpcId.THERMONUCLEAR_SMOKE_DEVIL_301));
    spawns.add(new NpcSpawn(8, new Tile(2371, 9452), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(8, new Tile(2357, 9454), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(8, new Tile(2356, 9445), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(8, new Tile(2363, 9443), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(8, new Tile(2370, 9444), NpcId.SMOKE_DEVIL_160));
    spawns.add(new NpcSpawn(8, new Tile(2366, 9455), NpcId.SMOKE_DEVIL_160));

    return spawns;
  }
}
