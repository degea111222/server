package com.palidinodh.maparea.kandarin.ancientcavern.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AncientCavernNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(1779, 5339, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1780, 5355, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1761, 5337, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1766, 5342, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1767, 5332, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1755, 5325, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1782, 5328, 1), NpcId.MITHRIL_DRAGON_304));
    spawns.add(new NpcSpawn(4, new Tile(1774, 5359), NpcId.BRUTAL_GREEN_DRAGON_227));
    spawns.add(new NpcSpawn(4, new Tile(1774, 5350), NpcId.BRUTAL_GREEN_DRAGON_227));
    spawns.add(new NpcSpawn(4, new Tile(1771, 5336), NpcId.BRUTAL_GREEN_DRAGON_227));
    spawns.add(new NpcSpawn(4, new Tile(1778, 5328), NpcId.BRUTAL_GREEN_DRAGON_227));
    spawns.add(new NpcSpawn(4, new Tile(1764, 5325), NpcId.BRUTAL_GREEN_DRAGON_227));
    spawns.add(new NpcSpawn(4, new Tile(1757, 5333), NpcId.BRUTAL_GREEN_DRAGON_227));
    spawns.add(new NpcSpawn(4, new Tile(1747, 5362), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1743, 5359), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1739, 5356), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1740, 5351), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1737, 5348), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1740, 5344), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1737, 5342), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1743, 5341), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1747, 5342), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1752, 5341), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1751, 5338), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1745, 5337), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1751, 5351), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1755, 5352), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1757, 5356), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1757, 5360), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1760, 5356), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(1762, 5359), NpcId.WATERFIEND_115));

    return spawns;
  }
}
