package com.palidinodh.maparea.morytania.mosleharmless;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14994, 14995, 15251})
public class MosLeHarmlessCaveArea extends Area {}
