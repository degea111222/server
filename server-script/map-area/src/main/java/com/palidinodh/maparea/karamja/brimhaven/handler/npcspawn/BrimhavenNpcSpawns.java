package com.palidinodh.maparea.karamja.brimhaven.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class BrimhavenNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(2766, 3215), NpcId.GARTH));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2764, 3210), NpcId.TOOL_LEPRECHAUN));

    return spawns;
  }
}
