package com.palidinodh.maparea.morytania.canifis.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CanifisHunterNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3539, 3450), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3535, 3448), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3530, 3444), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3538, 3448), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3535, 3446), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3549, 3451), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(8, new Tile(3545, 3450), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3556, 3453), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3555, 3451), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3550, 3452), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3548, 3449), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3561, 3437), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3558, 3434), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3561, 3431), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3554, 3437), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3551, 3440), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3550, 3438), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3546, 3439), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3548, 3439), NpcId.SWAMP_LIZARD));
    spawns.add(new NpcSpawn(4, new Tile(3554, 3438), NpcId.SWAMP_LIZARD));

    return spawns;
  }
}
