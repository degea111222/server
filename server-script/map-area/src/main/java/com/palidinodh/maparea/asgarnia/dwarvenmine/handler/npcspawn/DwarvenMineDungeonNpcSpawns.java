package com.palidinodh.maparea.asgarnia.dwarvenmine.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DwarvenMineDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3054, 9773), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(4, new Tile(3047, 9766), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(4, new Tile(3041, 9779), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(4, new Tile(3019, 9726), NpcId.BELONA));

    return spawns;
  }
}
