package com.palidinodh.maparea.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10037, 10038})
public class BaxtorianFallsArea extends Area {}
