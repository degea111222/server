package com.palidinodh.maparea.kandarin.ourania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12119)
public class OuraniaCaveArea extends Area {}
