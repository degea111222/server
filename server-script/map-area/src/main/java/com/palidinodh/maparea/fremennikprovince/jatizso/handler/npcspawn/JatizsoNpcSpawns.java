package com.palidinodh.maparea.fremennikprovince.jatizso.handler.npcspawn;

import com.palidinodh.osrscore.io.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class JatizsoNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(1, new Tile(2375, 3856), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2418, 3803), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(4, new Tile(2413, 3805), NpcId.MINER_1993));
    spawns.add(new NpcSpawn(4, new Tile(2411, 3807), NpcId.LENSA));
    spawns.add(new NpcSpawn(4, new Tile(2409, 3803), NpcId.THORKEL_SILKBEARD));
    spawns.add(new NpcSpawn(4, new Tile(2407, 3802), NpcId.KING_GJUKI_SORVOTT_IV));
    spawns.add(new NpcSpawn(4, new Tile(2406, 3802), NpcId.HRH_HRAFN));
    spawns.add(new NpcSpawn(4, new Tile(2405, 3808), NpcId.ERIC_1997));
    spawns.add(new NpcSpawn(4, new Tile(2417, 3815), NpcId.KEEPA_KETTILON));
    spawns.add(new NpcSpawn(4, new Tile(2417, 3813), NpcId.FLOSI_DALKSSON));
    spawns.add(new NpcSpawn(4, new Tile(2409, 3814), NpcId.BRENDT));
    spawns.add(new NpcSpawn(4, new Tile(2406, 3815), NpcId.GRUNDT));
    spawns.add(new NpcSpawn(4, new Tile(2414, 3820), NpcId.MINER_1993));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2416, 3799), NpcId.MAGNUS_GRAM));
    spawns.add(new NpcSpawn(4, new Tile(2396, 3797), NpcId.RAUM_URDA_STEIN));
    spawns.add(new NpcSpawn(4, new Tile(2398, 3804), NpcId.HRING_HRING));
    spawns.add(new NpcSpawn(4, new Tile(2398, 3797), NpcId.SKULI_MYRKA));
    spawns.add(new NpcSpawn(4, new Tile(2392, 3801), NpcId.MINER_1993));

    return spawns;
  }
}
