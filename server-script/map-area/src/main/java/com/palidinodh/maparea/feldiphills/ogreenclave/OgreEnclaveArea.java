package com.palidinodh.maparea.feldiphills.ogreenclave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10387)
public class OgreEnclaveArea extends Area {}
