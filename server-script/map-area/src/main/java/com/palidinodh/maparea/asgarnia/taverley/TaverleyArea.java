package com.palidinodh.maparea.asgarnia.taverley;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({11573, 11574})
public class TaverleyArea extends Area {}
