package com.palidinodh.maparea.asgarnia.molelair;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({6992, 6993})
public class MoleLairArea extends Area {}
