package com.palidinodh.io;

import com.palidinodh.rs.communication.ResponseServer;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.SecureSettings;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PString;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.util.logging.ExceptionLogger;

public class JavaCord {

  @Getter private static DiscordApi api = null;
  private static Map<String, Command> commands = new HashMap<>();

  public static void init(SecureSettings secureSettings, int worldId) {
    if (secureSettings.getDiscordToken() == null) {
      return;
    }
    new DiscordApiBuilder()
        .setToken(secureSettings.getDiscordToken())
        .login()
        .thenAccept(
            api -> {
              JavaCord.api = api;
              // System.out.println("Invite Url: " + api.createBotInvite());
              if (worldId == 1) {
                sendMessage(
                    Settings.getInstance().isLocal()
                        ? DiscordChannel.LOCAL
                        : DiscordChannel.ANNOUNCEMENTS,
                    Settings.getInstance().getName() + " is now online!");
              }
              TextChannel clanChatChannel = null;
              String clanChatChannelName =
                  Settings.getInstance().getDiscordChannel(DiscordChannel.CLAN_CHAT);
              if (worldId == -1 && clanChatChannelName != null) {
                TextChannel[] channels =
                    api.getTextChannelsByNameIgnoreCase(clanChatChannelName)
                        .toArray(new TextChannel[0]);
                if (channels.length == 1) {
                  clanChatChannel = channels[0];
                }
              }
              long clanChatChannelId = clanChatChannel != null ? clanChatChannel.getId() : -1;
              api.addMessageCreateListener(
                  event -> {
                    if (clanChatChannelId != -1
                        && event.getChannel().getId() == clanChatChannelId
                        && !event.getMessageAuthor().isBotUser()) {
                      String chatIcon = "[D]";
                      if (Settings.getInstance().getDiscordChatIcon() != -1) {
                        chatIcon = "<img=" + Settings.getInstance().getDiscordChatIcon() + ">";
                      }
                      String username =
                          PString.cleanName(event.getMessageAuthor().getDisplayName());
                      if (username.length() == 0) {
                        username = PString.cleanName(event.getMessageAuthor().getName());
                      }
                      if (username.length() > 0) {
                        String message = PString.removeHtml(event.getMessageContent());
                        message = PString.cleanString(message);
                        ResponseServer.getInstance()
                            .sendGlobalClanMessage(chatIcon + username, message);
                      }
                      return;
                    }
                    if (!event.getMessageAuthor().isServerAdmin()) {
                      return;
                    }
                    String message = event.getMessageContent();
                    if (!message.startsWith("::")) {
                      return;
                    }
                    String commandName = message.substring(2).split(" ")[0].toLowerCase();
                    Command command = commands.get(commandName);
                    if (command == null) {
                      return;
                    }
                    try {
                      int commandNameLength = 2 + commandName.length() + 1;
                      message =
                          commandNameLength < message.length()
                              ? message.substring(commandNameLength)
                              : "";
                      command.execute(event, message);
                    } catch (Exception e) {
                      e.printStackTrace();
                    }
                  });
            })
        .exceptionally(ExceptionLogger.get());
  }

  public static void sendMessage(DiscordChannel channel, String msg) {
    if (api == null || Settings.getInstance() == null) {
      return;
    }
    try {
      String channelName = Settings.getInstance().getDiscordChannel(channel);
      if (channelName == null) {
        return;
      }
      msg = msg.replaceAll("<.*>", "");
      if (msg.endsWith(" @everyone")) {
        int everyoneIndex = msg.lastIndexOf(" @everyone");
        msg = "```" + msg.substring(0, everyoneIndex) + "``` @everyone";
      } else {
        msg = "```" + msg + "```";
      }
      TextChannel[] channels =
          api.getTextChannelsByNameIgnoreCase(channelName).toArray(new TextChannel[0]);
      if (channels.length == 0) {
        return;
      }
      new MessageBuilder().append(msg).send(channels[0]);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void disconnect() {
    if (api == null) {
      return;
    }
    api.disconnect();
    api = null;
  }

  public static void addCommand(String name, Command command) {
    commands.put(name, command);
  }

  public interface Command {

    void execute(MessageCreateEvent event, String message);
  }
}
