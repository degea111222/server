package com.palidinodh.rs.adaptive;

import com.palidinodh.util.PString;

public enum RsGameMode {
  UNSET,
  REGULAR,
  IRONMAN,
  HARDCORE_IRONMAN,
  GROUP_IRONMAN,
  DEADMAN;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace("_", " "));
  }
}
