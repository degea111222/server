package com.palidinodh.rs.adaptive;

import com.palidinodh.util.PString;

public enum RsDifficultyMode {
  UNSET,
  NORMAL,
  HARD,
  ELITE;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace("_", " "));
  }
}
