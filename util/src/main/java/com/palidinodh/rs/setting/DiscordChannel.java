package com.palidinodh.rs.setting;

public enum DiscordChannel {
  ANNOUNCEMENTS,
  GAME_ANNOUNCEMENTS,
  GENERAL,
  CLAN_CHAT,
  MODERATION,
  LOCAL
}
